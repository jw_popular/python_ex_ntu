'''
說明
假定某班有5位學生,每位學生各修3門科目,請利用二維陣列的方式儲存學生的各科成績,並將每位學生的各科成績,
總分及平均列印出來,並按照找出班上最平均高分的學生。

Input Format
學生1的3科成績
學生2的3科成績
...
學生5的3科成績

Output
學生1
科目成績1 科目成績2 科目成績3
總分
平均
...
學生5
科目成績1 科目成績2 科目成績3
總分
平均(小數點後2位)
班總分 校平均(含換行)
平均最高分的學生與平均分數(小數點後2位)

Output
...
...
student 5
 1: 72
 2: 74
 3: 73
 sum: 219
 avg: 73.00
total: 1225, avg: 81.67
highest avg: student 3: 88.67

'''

lst = []

total_sum = 0
total_avg = 0

highest_avg = 0
highest_s = 0

for i in range(5):
    lst.append(input().split())


for i in range(5):
    s_sum = 0
    s_avg = 0
    print("student",i+1)
    tmp = lst[i].copy()
    for x in range(len(tmp)):
        tmp[x]=int(tmp[x])
        s_sum += tmp[x]
        print(" %d: %d"%(x+1, tmp[x]))
    print(" sum: %d"%s_sum)

    s_avg = s_sum/len(tmp)
    print(" avg: %.2f"%s_avg)

    if s_avg>=highest_avg:
        highest_avg = s_avg
        highest_s = i+1

    total_sum += s_sum
    total_avg += s_avg

total_avg = total_avg/5

print("total: %d, avg: %.2f"%(total_sum, total_avg))
print("highest avg: student %d: %.2f"%(highest_s, highest_avg))

#print(lst)



