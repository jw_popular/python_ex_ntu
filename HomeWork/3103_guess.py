'''
說明

(本題圖片參考用)

但可惜的是系統無法批改亂數題目
因此 此亂數由使用者手動輸入

Input Format
第一行為預設答案ans(2<=ans<=99)
之後的N行為要猜的數字guess(2<=ans<=99) 2-99

Output Format
猜對的話輸出”bingo answer is x”
猜錯則反之輸出”wrong answer, guess larger”
或是”wrong answer, guess smaller”
如果猜的數字不在範圍內(min<guess<max)要出出"out of range"並重新輸入
'''

ans=int(input())

if not (2<=ans<=99):
    ans=45

min=1
max=100
print(min,"<","?","<",max)

while True:
    g=int(input())
    if not (min<g<max):
        print("out of range")
    elif (g!=ans):
        if g>ans:
            print("wrong answer, guess smaller")
            max=g
        elif g<ans:
            print("wrong answer, guess larger")
            min=g
    else:
        print("bingo answer is",g)
        break
    print(min,"<","?","<",max)