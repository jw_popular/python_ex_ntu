'''
說明
請讓使用者依序輸入數字，直到輸入q停止。


列印出浮點數之乘積的和(所有浮點數相乘)。
列印出整數之乘積的和(所有整數相乘)。
比較上面兩者的大小，並列印出"Float > Int"或"Float = Int"或"Float < Int"

Input Format
int or float
...
...
q

Output Format
列印出浮點數之乘積的和(所有浮點數相乘) (取四捨五入到小數點第二位)
列印出整數之乘積的和(所有整數相乘)
"Float > Int"或"Float = Int"或"Float < Int"

'''

lst = []

while(True):
    val = input()
    if(val=='q'):
        break
    lst.append(eval(val))

sum_float = 1
sum_int = 1
for i in range(len(lst)):
    if(type(lst[i])==float):
        sum_float *= lst[i]
    if(type(lst[i])==int):
        sum_int *= lst[i]

print("%.2f"%sum_float)
print("%d"%sum_int)

if(sum_float>sum_int):
    print("Float > Int")
elif(sum_float<sum_int):
    print("Float < Int")
else:
    print("Float = Int")
