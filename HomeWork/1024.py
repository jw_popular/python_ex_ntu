'''
說明
山姆終于啓程去登山了。過程中山姆不小心迷路了!
但是山姆并不害怕，因爲他有帶足夠的糧食和通訊裝備，所以他想繼續走走看看。
走著走著誤入了一片松樹林。偶然間他也發現了一個部落。
這個部落叫做鳩琪部落。
那裏的人主要工作都是砍伐松樹然後裝飾成聖誕樹然後賣出。
部落裏的人對山姆甚是熱情，讓山姆體驗到了濃濃的人情味。
爲了表示感謝，山姆打算幫他們一起佈置聖誕樹。
但是他需要一個藍圖才能為不同高度的樹進行修剪，所以就靠你來幫忙了。

請試著輸入一數字n , 並印出一棵豪華聖誕樹。
樹葉由三層一組，底部由 n2-1 的'^'開始建立起來。
然後建了三層后就變成 (n-1)2-1 的 '^'。
重複動作直到最上層的 '^'。
'^'號個數為奇數。
最頂部有一顆'*'號。
樹印好后還有高度為 n-2 ,厚度為 n-2 的 樹幹。符號為'#'。

Input Format
整數n (n>1,n%2==1)

Output Format
印出豪華聖誕樹。每行後無空白。

n=5
    *
    ^
   ^^^
  ^^^^^
   ^^^
  ^^^^^
 ^^^^^^^
  ^^^^^
 ^^^^^^^
^^^^^^^^^
   ###
   ###
   ###
'''


n = int(input())

top = 1
top_empty = n-1
level_s = 1
level = n-2
leaf_level = 3
root = n-2
root_level = n-2
root_empty = int((n*2-1-(n-2))/2)

for i in range(1, n+1):
    if(i==1):
        print((" " * top_empty) + ("*" * top))
    elif(i==n):
        for j in range(root_level):
            print((" " * root_empty) + ("#" * root_level))
    else:
        s = level+i
        for k in range(leaf_level):
            e = s-level_s-k
            print((" " * e)+("^" * (level_s+k*2)))
        level_s+=2



