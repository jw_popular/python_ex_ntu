'''
Task Description
Write a program to read two integers a and b, then print b first in a line, 
then print a in another line.

Input Format
There are two lines in the input. The first line has the integer a and 
the second line has the integer b.

Output Format
There are two lines in the output. The first line has the integer b and 
the second line has the integer a.
'''

a = int(input())
b = int(input())

print(b)
print(a)
