##################################################################
# Excel to XML tool
# So far, only support two languages transfer
# And only for project using
# input file: cus_string_translate.xlsx
# output file: new_dictionary.xml
##################################################################

import os
import sys
import platform
import shutil

from xml.dom import minidom
from openpyxl import load_workbook  # pip install openpyxl
from openpyxl import Workbook

import xml.etree.cElementTree as ET

import_file = os.getcwd() + "/excel_string_translate.xlsx"

temp_file = os.getcwd() + "/tmp_excel_string_translate.xlsx"

output_xml_file = os.getcwd() + "/xml_dictionary.xml"

os_name = platform.system()

if os_name=="Linux":
    print("OS:",os_name)
elif os_name=="Windows":
    print("OS:",os_name)
else:
    sys.exit("Unknown OS: " + os_name)
    exit(1)

# append newline and tab character for XML
def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

#------------------------------------------------------------------------------------------
shutil.copyfile(import_file, temp_file)

wb = load_workbook(temp_file)
firstSheet = wb.sheetnames[0]
#print(firstSheet)

if firstSheet == "translation":
    ws = wb[firstSheet]
print("Work Sheet:",ws.title)

#for row in ws.iter_rows(min_row=1, max_col=3):
#    for cell in row:
#        print(cell.value)

#for col in ws.iter_cols(min_row=1, max_col=3, max_row=2):
#    for cell in col:
#        print(cell)

root = ET.Element("dictionary", ver = "1.0",)
ET.SubElement(root, "option").text="eng"
ET.SubElement(root, "option").text="cht"

keepGroup = ""
keepPage = ""
for row in ws.iter_rows(min_row=2, values_only=True):
    if keepGroup != row[2]:
        group = ET.SubElement(root, "group", name = row[2])
        keepGroup = row[2]
    if keepPage != row[3]:
        if row[3] != None:
            page = ET.SubElement(group, "page", name = row[3])
        keepPage = row[3]
    #print(row[3])
    if row[3] == None:
        translations = ET.SubElement(group, "translations")
    else:
        translations = ET.SubElement(page, "translations")

    translations.set("name", row[4])
    if row[5] != None:
        translations.set("arguments", str(row[5]))
    ET.SubElement(translations, "translation", language = "eng").text=row[0]
    ET.SubElement(translations, "translation", language = "cht").text=row[1]
    

tree = ET.ElementTree(root)
indent(root)
tree.write(output_xml_file, xml_declaration=True, encoding="UTF-8", method="xml")

os.remove(temp_file)

print("Done!!")
