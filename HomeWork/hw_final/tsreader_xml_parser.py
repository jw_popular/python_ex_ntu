##################################################################
# XML to Excel tool
#  
# input file:  the xml files from TSReader exported
# output file: output_excel_tsreader.xlsx
#
##################################################################
#from ast import IsNot
#from cmath import isnan
import os
import sys
import platform
import shutil

from xml.dom import minidom
#from openpyxl import load_workbook
from openpyxl import Workbook  # pip install openpyxl

from os.path import exists

#import_file = ("NIT_add_261M.xml", "NIT_add_267M.xml", "NIT_add_273M.xml")
import_file = ("NIT_add_261M.xml")

temp_file = os.getcwd() + "/tmp_tsreader.xml"

output_excel_file = os.getcwd() + "/output_excel_tsreader.xlsx"

os_name = platform.system()


if os_name=="Linux":
    print("OS:",os_name)
    print(os.getcwd())
elif os_name=="Windows":
    print("OS:",os_name)
    print(os.getcwd())
else:
    sys.exit("Unknown OS: " + os_name)
    exit(1)


def parse_xml(input_file):
    
    iFile = os.getcwd() + "/" + input_file

    shutil.copyfile(iFile, temp_file)

    f = open(temp_file,"r",encoding = "utf-8")

    r = f.read()

    text = str(r.encode('utf-8'),encoding = "utf-8")

    DOMTree = minidom.parseString(text)

    collection = DOMTree.documentElement

    #collectionStr = "Collection:"+collection.nodeName
    #print(collectionStr)

    tb = ("PAT", "PMTs", "SDT", "NIT")

    table_name = collection.getElementsByTagName(tb[3])

    if table_name:
        #print(tb[3], "is exist!")
        pass
    else:
        print(tb[3], "is empty!")
        print("!!!Wrong!!!")
        f.close()
        os.remove(temp_file)
        exit(1)

    if table_name.length != 1:
        print("Multiple NITs")

    for t in table_name:
        # ELEMENT_NODE, ATTRIBUTE_NODE, TEXT_NODE, CDATA_SECTION_NODE, 
        # ENTITY_NODE, PROCESSING_INSTRUCTION_NODE, COMMENT_NODE, 
        # DOCUMENT_NODE, DOCUMENT_TYPE_NODE, NOTATION_NODE
        nit_entry_cnt = 0
        for n_list in t.childNodes:
            if n_list.nodeType == minidom.Node.ELEMENT_NODE:
                lst = []
                if n_list.nodeName == "VERSION":
                    print("NIT Version:", n_list.firstChild.data)
                    lst.append("NIT Version")
                    lst.append(n_list.firstChild.data)
                    if len(n_list.childNodes) != 1:
                        print("??? Multiple NIT Version ???")
                        lst.append("Multi-Version")
                    actSheet.append(lst)
                elif n_list.nodeName == "NIT-PID":
                    print("NIT PID:", n_list.firstChild.data)
                    lst.append("NIT PID")
                    lst.append(n_list.firstChild.data)
                    if len(n_list.childNodes) != 1:
                        print("??? Multiple NIT PID ???")
                        lst.append("Multi-PID")
                    actSheet.append(lst)
                elif n_list.nodeName == "NIT-ENTRY":
                    if nit_entry_cnt == 0:
                        lst_title = ["INDEX", "NID", "TS ID", "ONID", "FREQ", "SERVICE-ID"]
                        actSheet.append(lst_title)
                    nit_entry_cnt += 1
                    n_val = []
                    for n_entry in n_list.childNodes:
                        if n_entry.nodeName == "INDEX":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "NETWORK-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "TRANSPORT-STREAM-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "ORIGINAL-NETWORK-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "FREQUENCY":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "TS-DESCRIPTORS":
                            for n_des in n_entry.childNodes:
                                if n_des.nodeName == "SERVICE-LIST":
                                    n_serv = n_des.getElementsByTagName("SERVICE")
                                    for n_s in n_serv:
                                        n_serv_id = n_s.getElementsByTagName("ID")
                                        for n_s_id in n_serv_id:
                                            #print(n_s_id.firstChild.data)
                                            lst.append(n_s_id.firstChild.data)
                                else:
                                    pass
                            actSheet.append(lst)
                        else:
                            if n_entry.nodeType != minidom.Node.TEXT_NODE:
                                pass
                else:
                    if n_list.nodeName == "CRC-ERRORS":
                        pass
                    else:
                        print(n_list.nodeName)
            else:
                if n_list.nodeType != minidom.Node.TEXT_NODE:
                    print("Other node:", n_list.nodeName)

        print("NIT Entry numbers:", nit_entry_cnt)

    f.close()
    os.remove(temp_file)


wb = Workbook()

for k in range(len(import_file)):
    iFile = os.getcwd() + "/" + import_file[k]
    if exists(iFile):
        if k==0:
            actSheet = wb.active
            actSheet.title = import_file[k]
        else:
            wb.create_sheet(import_file[k])
            actSheet = wb[import_file[k]]
        print("Work Book:",actSheet.title)
        parse_xml(import_file[k])

wb.save(output_excel_file)

print("Done!!")