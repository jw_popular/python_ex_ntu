##################################################################
# XML to Excel & CSV tool
# So far, only support two languages transfer
# And only for SEHC-1910NK project using
# input file: dictionary.xml
# output file: output_string_translate.xlsx & string_translate.csv
##################################################################
import os
import sys
import platform
import shutil

from xml.dom import minidom
#from openpyxl import load_workbook
from openpyxl import Workbook  # pip install openpyxl

import time

import_file = os.getcwd() + "/dictionary.xml"

temp_file = os.getcwd() + "/tmp_dictionary.xml"

output_csv_file = os.getcwd() + "/output_csv_string_translate.csv"

output_excel_file = os.getcwd() + "/output_excel_string_translate.xlsx"

os_name = platform.system()

if os_name=="Linux":
    print("OS:",os_name)
elif os_name=="Windows":
    print("OS:",os_name)
else:
    sys.exit("Unknown OS: " + os_name)
    exit(1)

start = time.time()

shutil.copyfile(import_file, temp_file)

f = open(temp_file,"r",encoding = "utf-8")
fp = open(output_csv_file,"w",encoding = "utf-8-sig")

wb = Workbook()
actSheet = wb.active
#print(actSheet.title)
actSheet.title = "translation"
print("Work Book:",actSheet.title)


r = f.read()

text = str(r.encode('utf-8'),encoding = "utf-8")

DOMTree = minidom.parseString(text)

collection = DOMTree.documentElement

collectionStr = "Collection:"+collection.nodeName
print(collectionStr, file = fp)
#lst = [collectionStr]
#actSheet.append(lst)

option = collection.getElementsByTagName("option")

#print(len(option))
#print("Option ==>", file = fp)
strOption = "Option:"
i = len(option)
for x in option:
 #   print(x)
 #   print(x.firstChild)
    i -= 1
    #print(x.firstChild.data, file = fp)
    if i!=0:
        strOption = strOption + x.firstChild.data + ";"
    else:
        strOption = strOption + x.firstChild.data
#print("----------------------------")
print(strOption, file = fp)
#lst = [strOption]
#actSheet.append(lst)

groups = collection.getElementsByTagName("group")

print(str(len(groups))+" GROUPS", file = fp)
print("---")
#print("---")
print("Engligh,Chinese,Groups,Pages,Translations", file = fp)
lst = ['English', 'Chinese', 'Groups', 'Pages', 'Translations', 'Arguments']
actSheet.append(lst)
for g in groups:
    if g.hasAttribute("name"):
        #print("Group["+g.getAttribute("name")+"]", file = fp)
        groupName = g.getAttribute("name")
        pages = g.getElementsByTagName("page")
        #print("    "+str(len(pages))+" PAGES", file = fp)
        if len(pages) == 0:
            #print(groupName)
            translations = g.getElementsByTagName("translations")
            #print("        "+str(len(translations))+" TRANSLATIONS", file = fp)
            pageName = ""
            for t in translations:
                argumentsValue = ""
                if t.hasAttribute("arguments"):
                    argumentsValue = str(t.getAttribute("arguments"))
                if t.hasAttribute("name"):
                    #print("        Translations["+t.getAttribute("name")+"]")
                    translationsName = t.getAttribute("name")
                    translation = t.getElementsByTagName("translation")
                    #if len(translation)!=3:
                    #    #print("            "+str(len(translation))+" TRANSLATION", file = fp)
                    #    print("========>>>"+str(len(translation))+" TRANSLATION")
                    engStr = ""
                    chiStr = ""
                    for l in translation:
                        if l.hasAttribute("language"):
                            bold = l.getElementsByTagName("b")
                            if len(bold)>0:
                                print(groupName+","+pageName+","+translationsName)
                                #print(translation[0].text)
                            #else:
                            if l.getAttribute("language")=="eng":
                                #print("            "+l.firstChild.data)
                                engStr = l.firstChild.data
                            if l.getAttribute("language")=="cht":
                                #print("            "+l.firstChild.data)
                                chiStr = l.firstChild.data
                    print("\""+engStr+"\""+","+"\""+chiStr+"\""+","+groupName+","+pageName+","+translationsName, file = fp)
                    lst = [engStr, chiStr, groupName, pageName, translationsName, argumentsValue]
                    actSheet.append(lst)
        else:
            for p in pages:
                if p.hasAttribute("name"):
                    #print("    Page["+p.getAttribute("name")+"]", file = fp)
                    pageName = p.getAttribute("name")
                    translations = p.getElementsByTagName("translations")
                    #print("        "+str(len(translations))+" TRANSLATIONS", file = fp)

                    for t in translations:
                        argumentsValue = ""
                        if t.hasAttribute("arguments"):
                            argumentsValue = str(t.getAttribute("arguments"))
                        if t.hasAttribute("name"):
                            #print("        Translations["+t.getAttribute("name")+"]", file = fp)
                            translationsName = t.getAttribute("name")
                            translation = t.getElementsByTagName("translation")
                            #if len(translation)!=3:
                            #    #print("            "+str(len(translation))+" TRANSLATION", file = fp)
                            #    print("========>>>"+str(len(translation))+" TRANSLATION")

                            engStr = ""
                            chiStr = ""
                            for l in translation:
                                if l.hasAttribute("language"):
                                    if l.getAttribute("language")=="eng":
                                        #print("            "+l.firstChild.data, file = fp)
                                        engStr = l.firstChild.data
                                    if l.getAttribute("language")=="cht":
                                        #print("            "+l.firstChild.data, file = fp)
                                        chiStr = l.firstChild.data
                            print("\""+engStr+"\""+","+"\""+chiStr+"\""+","+groupName+","+pageName+","+translationsName, file = fp)
                            lst = [engStr, chiStr, groupName, pageName, translationsName, argumentsValue]
                            actSheet.append(lst)

fp.close()
f.close()

wb.save(output_excel_file)

os.remove(temp_file)

print("Duration: %f Secs" % (time.time()-start))

print("Done!!")
