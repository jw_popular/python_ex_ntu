'''
說明
Armstrong number (阿姆斯壯數)是指一個n位數的整數，它的所有位數的n次方和剛好等於自己。

例一：407 = 4^3 + 0^3 + 7^3
例二：1634 = 1^4 + 6^4 + 3^4 + 4^4

現在題目書入一個範圍，請找出該範圍內所有的armstrong number。

Input Format
n m (n、m兩整數由空格格開；且n0,m<=100000)

Output Format
若該範圍有值列印：int & int & int...
若該範圍找不到阿姆斯壯數則列印：none

'''

n, m = input().split()
lst = []
s = ''

for i in range(int(n),int(m)+1):
    l = len(str(i))
    v = str(i)
    val = 0

    #print("l = ", l)
    for j in range(l):
        d = int(v[j])
        val += d**l
    if(i == val):
        if(s == ''):
            s = str(val)
        else:
            s = s + " & " + str(val)

if(s == ''):
    print("none")
else:
    print(s)
