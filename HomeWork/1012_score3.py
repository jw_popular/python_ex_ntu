'''
說明
寫個程式，判斷一個人的成績是否及格
(大學部及格分數為60分，研究所為70分)
令使用者輸入學籍與成績
若是學籍使用者輸入的不是1也不是2便輸出錯誤訊息role error並結束程式
若是分數使用者輸入的不是介於0~100便輸出錯誤訊息score error並結束程式
若以上輸入皆正確就判斷其成績是否及格並輸出

Input Format
第一行輸入一整數學藉(1:大學部 2:研究所)
如果第一行輸入在許可範圍內，可輸入第二行為一整數成績，如果第一行輸入的學藉不在範圍內，不可有第二行輸入

Output Format
若學籍有錯誤就輸出role error並結束程式
若分數有錯誤就輸出score error並結束程式
若皆正確就輸出pass或fail

Hint
若輸入錯誤的學籍顯示「role error」後，程式沒有結束即表示流程有問題，是無法通過批改的！

'''

role=int(input())

if role==1 or role==2:
    score=eval(input())
    if 0<=score<=100:
        if (role==1 and score>=60) or (role==2 and score>=70):
            print("pass")
        else:
            print("fail")
    else:
        print("score error")
else:
    print("role error")
