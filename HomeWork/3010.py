'''
小練習 說明
質數判斷程式
請撰寫一程式，令使用者輸入一整數n
並判斷n是否為質數 "is not prime" or "is prime"

Input Format
數字n

Output Format
是否為質數

'''

n = int(input())

fct=0

for i in range(1, n+1):
    if(n%i==0):
        fct+=1

if(fct>2):
    print(n, "is not prime")
else:
    print(n, "is prime")
