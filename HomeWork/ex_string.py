s1 = '  apple    \t      \t     \n   '
print("s1 = ", s1)
s2 = s1.strip()
print("s2 = ", s2)
print(s1=='apple')
print(s2=='apple')

print(s1)
print(s1.strip())
if s1=='apple':
    print('s1 is an apple')
else:
    print("s1 is not apple")

print(s1)
print(s1.endswith("apple"))
print(s1.endswith("\n"))
print(s1.endswith(" "))

