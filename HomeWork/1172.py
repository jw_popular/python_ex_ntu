'''
說明
請宣告一個整數陣列，內容有5格
輸入這5格陣列的內容後
請將陣列內容排序後依序印出陣列內容(整數數字)後空一tab(\t)印出該數字個數個*號

Input Format
整數1
整數2
整數3
整數4
整數5

Output Format
整數(tab間隔)整數1個*號(換行)
整數(tab間隔)整數2個*號(換行)
整數(tab間隔)整數3個*號(換行)
整數(tab間隔)整數4個*號(換行)
整數(tab間隔)整數5個*號(換行)
(由小到大依序列印)
'''

lst = []

for i in range(5):
    lst.append(int(input()))

lst.sort()

for i in range(5):
    print("%d\t%s"%(lst[i],"*"*lst[i]))