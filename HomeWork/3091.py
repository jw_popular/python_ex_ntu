'''
說明
試寫一個名為student的類別
其中屬性包含:
name, gender, grades
方法(函數)包含:
__init__(name,gender)：在建立實體實傳入 姓名及性別
avg(): 回傳grades list的平均值,回傳一浮點數
add(grade): 新增成績到grades list中,無回傳值
fcount(): 回傳不及格(<60)的總數，為一整數
show(): 可以將學生資料依格式列印出來，無回傳值
以上不要忘記傳入self

'''

class student:
    def __init__(self, n, gend) -> None:
        self.name=n
        self.gender=gend
        self.grades = []
    def avg(self):
        return sum(self.grades)/len(self.grades)
    def add(self, val):
        self.grades.append(val)
    def fcount(self):
        ret = 0
        for i in range(len(self.grades)):
            if self.grades[i]<60:
                ret+=1
        return ret
    def show(self):
        print(self.name)
        print("%.2f"%self.avg())
        print(self.fcount())

n_ame = input()
g_ender = input()

st = student(n_ame, g_ender)

for i in range(3):
    st.add(eval(input()))

st.show()



