'''
說明
請試著輸入一數字n並印出一倒三角形

Input Format
數字n

Output Format
高為n的*號倒三角形

'''

n = int(input())

for i in range(n):
    print("%s%s"%((' ' * i), ('*' * (n-i))))

