'''
小練習 說明
請撰寫一程式，列印出一半徑為100公分的圓形之圓週率，半徑，圓周長及圓面積

Input Format
no

Output Format
圓周率
半徑(整數)
圓周長
圓面積

Hint
import math
math.pi

'''

import math

a=100
print('圓周率 ', math.pi)
print('半徑', a)
print('圓周長', a*2*math.pi)
print('圓面積', a**2*math.pi)

