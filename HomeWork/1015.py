'''
說明
輸入鍵盤按鍵輸入一字元後，立即印出你所輸入的字元，直到輸入q後程式結束(q也要印出來)。

Input Format
字元1
字元2
... q

Output Format
字元1
字元2
... q

'''

while(True):
    i = input()
    print(i)

    if(i=='q'):
        break
