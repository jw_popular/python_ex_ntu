i = 10

for j in range(3, -4, -1):
    try:

        print("%d/%d=%0.3f"%(i, j, i/j))

    except ZeroDivisionError:
        print ("Zero Division Error")

    except:
        print("Unknown Error")
    else:
        print("-------")
    
    finally:
        print("*******")