# only support on python 3.x
import math

a = '{}+{}={}'
print(a)

print(a.format(1,4,3))

b = '{city},{country}'

print(b)

print(b.format(country="Taiwan", city="Taoyuan"))
#print(b.format(Country="Taiwan", City="Taoyuan")) # Will sahow error, caused by case sense

c = '{2}+{0}={1}'
print(c)

print(c.format(4,5,2))

s1=3.456789
s2=-2
s3=math.pi
s4=-123.511493

print('<{0:6.2f}> <{1:.2f}> <{2:+08.2f}> <{3:-.3f}>'.format(s1, s3, s2, s4))