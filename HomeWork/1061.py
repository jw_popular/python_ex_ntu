'''
小練習 說明
請撰寫一程式，可以令使用者輸入一大於0的整數n，算出1加到n的結果
此程式，內含一可以計算1+2+3…+n的函式sum1
Python: def sum1(n):，Type Hint:def sum1(n: int)->int:
C/C++: int sum1(int n);
可以回傳一整數為1到n加總之和，並呼叫使用此函式，在程式中將此回傳值印出，請確保此函式可以被重複呼叫使用而不會錯誤。

Input Format
數字n(0<=n<=10)

Output Format
1加到 n 的總和
'''

def sum1(n: int):
    val = 0
    for i in range(1, n+1):
        val += i
    return val

n = int(input())

print(sum1(n))