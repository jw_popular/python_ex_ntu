'''
波西傑克森的閱讀障礙
說明
波賽頓之子波西傑克森擁有能夠操控水體的能力，在眾神中是能力甚強的混血孩子之一。
然而在進入混血營之前，他的童年非常不順遂。 
自小被診斷出閱讀障礙以及過動症，導致他進入混血營之前一直有學習困難，是老師眼中的問題學生。 
深深相信他潛力的老師aka你，嘗試著理解他閱讀時的思路，有了以下的發現：

所有他看到的數字（二進位制）都會被左右顛倒。

舉例而言：
假設一個十進位數字13(在二進位中是00000000000000000000000000001101)，
其反轉後的結果為10110000000000000000000000000000(在十進位中是2952790016)
因此數字13會被波西傑克森看成2952790016。

保證題目給定的十進位數字皆為可用32位元表示的非負整數。 請輸出波西傑克森解讀到的數字轉換為十進位非負整數後的結果。

PS. 禁止使用 if while for

##解析ppt: https://docs.google.com/presentation/d/1FFLm93ufIgWWsKLj00UxP4l5EXHMc6PsQ-tkfgUbYcE/edit?usp=sharing

Input Format
輸入為一個十進位非負整數N。保證 N可以被表示為 32 位元的二進位數(不足32位請補0)。

Output Format
請輸出一個十進位非負整數，代表波西傑克森解讀到的數字轉換後的結果。


'''
n = eval(input())

swap_1 = '0b10101010101010101010101010101010'
swap_2 = '0b11001100110011001100110011001100'
swap_4 = '0b11110000111100001111000011110000'
swap_8 = '0b11111111000000001111111100000000'
swap_16 = '0b11111111111111110000000000000000'


n2 = (n & int(swap_1, 2)) >> 1
#print(n2, " = ", bin(n2))
n3 = (n & (int(swap_1, 2) >> 1)) << 1
#print(n3, " = ", bin(n3))
n = n2 | n3

#print(n, " = ", bin(n))

n5 = (n & int(swap_2, 2)) >> 2
#print(n5, " = ", bin(n5))
n6 = (n & (int(swap_2, 2) >> 2)) << 2
#print(n6, " = ", bin(n6))
n = n5 | n6

#print(n, " = ", bin(n))

n7 = (n & int(swap_4, 2)) >> 4
#print(n7, " = ", bin(n7))
n8 = (n & (int(swap_4, 2) >> 4)) << 4
#print(n8, " = ", bin(n8))
n = n7 | n8

#print(n, " = ", bin(n))

n9 = (n & int(swap_8, 2)) >> 8
#print(n9, " = ", bin(n9))
n10 = (n & (int(swap_8, 2) >> 8)) << 8
#print(n10, " = ", bin(n10))
n = n9 | n10

#print(n, " = ", bin(n))

n11 = (n & int(swap_16, 2)) >> 16
#print(n11, " = ", bin(n11))
n12 = (n & (int(swap_16, 2) >> 16)) << 16
#print(n12, " = ", bin(n12))
n = n11 | n12

#print(n, " = ", bin(n))
print(n)




