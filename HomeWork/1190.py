'''
說明
請撰寫一程式，令使用者可以輸入一字串並讓程式判斷此字串的類型，有以下類型：

全部都是大寫的(可以有標點符號)
全部都是小寫的(可以有標點符號)
只有英文字母的，但不是全大寫也不是全小寫的(不可以有標點符號)
只有英文字母+數字的(不可以有標點符號)
只有數字的(不可以有標點符號)
只有white-space的
以上這些都不是的
Input Format
字串s 長度<128字元

Output Format
判斷結果

全部都是大寫的輸出 "x" is a uppercase string.，並將轉換成小寫輸出 swap to lowercase string "X".
全部都是小寫的輸出 "x" is a lowercase string.，並將轉換成大寫輸出 swap to uppercase string "X".
只有英文字母的，但不是全大寫也不是全小寫的輸出 "x" is an alphabet string.
只有英文字母和數字的輸出 "x" is an alphanumeric string.
只有數字的輸出 "x" is a digital string.
只有white-space的輸出 "x" is a string that all the characters are whitespaces.
以上這些都不是的輸出 "x" is a normal string.

'''
