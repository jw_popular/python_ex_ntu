'''
小練習 說明
請撰寫一程式，可以令使用者輸入一整數n，算出n階乘的結果
並撰寫一個可以計算123…*n的n!函式fact置於同一程式之中 並呼叫使用此函式利用此函式計算出結果，在程式中將此回傳值印出，請確保此函式可以被重複呼叫使用而不會錯誤。
Python: def fact(n):，Type Hint:def fact(n: int)->int:
C/C++: int fact(int n);
最後回傳一整數為n階乘的結果
在程式中將此回傳值印出

Input Format
數字n (0<=n<=10)

Output Format
1乘到 n 的總和
'''
def fact(n: int):
    val = 1
    for i in range(1, n+1):
        val *= i
    return val

n = int(input())

print(fact(n))