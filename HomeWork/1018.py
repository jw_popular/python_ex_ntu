'''
說明
寫個程式，判斷一個人的成績是否及格 (及格分數為60分)
令使用者輸入成績
並判斷其成績是否及格
每次判斷完成績後可輸入
'y': 繼續
其他: 離開

Input Format
成績
是否繼續

Output Format
pass或fail
'''

while(True):
    score=eval(input())
    if(score>=60):
        print("pass")
    else:
        print("fail")
    
    cont=input()
    if(cont!='y'):
        break