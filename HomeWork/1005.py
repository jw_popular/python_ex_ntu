'''
說明
請撰寫一個可以獨立執行的程式，可以是hello.c/hello.cpp/hello.py類型的檔案
使其可以在螢幕上列印出下列指定文字
※注意文字內容要一模一樣(包含大小寫、空白與標點符號都需相同)
※注意請上傳上述之程式檔，勿傳*.exe執行檔、勿傳*.dev專案檔、勿把程式執行結果畫面及文字存下來上傳

Input Format
本題沒有輸入

Output Format
二行指定文字，注意不要輸出多餘的文字和空白字元！
'''

print('Hello','World!')
print('This is my first program.')