'''
Task Description
請撰寫一程式，令使用者輸入一數字N，接著連續輸入N個數字
請找出，這些數字的最大值、最小值及其位置。
Input format
第一行輸入整數N，0≤N≤1000000
接下來1~N行，輸入均為整數，0≤1~N≤1000000

Output format
最大值(整數) 第一個最大值的位置(整數) 最後一個最大值的位置(整數，如果有多個的話才輸出，沒有的話不用)
最小值(整數) 第一個最小值的位置(整數) 最後一個最小值的位置(整數，如果有多個的話才輸出，沒有的話不用)

'''

n = int(input())

max = -1
max_pos = [0, 0]
min = 1000001
min_pos = [0, 0]

for i in range(n):
    val = int(input())
    if(val == max):
        max_pos[1]=i+1
    if(val > max):
        max=val
        max_pos[0]=i+1
        max_pos[1]=0

    if(val == min):
        min_pos[1]=i+1
    if(val < min):
        min = val
        min_pos[0]=i+1
        min_pos[1]=0

if(max_pos[1]==0):
    print(max, max_pos[0])
else:
    print(max, max_pos[0], max_pos[1])
if(min_pos[1]==0):
    print(min, min_pos[0])
else:
    print(min, min_pos[0], min_pos[1])
