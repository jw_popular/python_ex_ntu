'''
說明
使用者可以輸入二字串
比較這兩字串文字的大小(ex:"cde">"abc") ※注意，並非比較字串長度
將此二字串連接起來後輸出
並計算連接後的字串長度

Input Format
字串一
字串二

Output Format
比較：1>2、1==2、1<2
二字串連接後輸出
連接後的字串長度 (不含換行)
'''

s1=input()
s2=input()

if s1>s2:
    print("1>2")
elif(s1==s2):
    print("1==2")
else:
    print("1<2")

ss=s1+s2
print(ss)

print(len(ss))