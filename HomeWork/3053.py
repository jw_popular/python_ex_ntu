'''
說明
金字塔是世界七大奇景之一，請利用for迴圈寫出如下圖所示的N層金字塔圖形。

Input Format
數字N

Output Format
高為n的*號金字塔(每列最後/右一個星號後無空白)

   *
  * *
 * * *
* * * *

'''


n = int(input())

s = " *"

for i in range(1, n+1):
    e = n-1-i
    if(e==-1):
        print("*" + s * (i+e))
        break
    else:
        print(" " * e + s * i)

#print(n, e)

#print("test", end=' ')

#print("TEST")
