'''
說明
請撰寫一程式，令使用者輸入一數字T代表希望比對的數字，接著輸入一數字N代表接下來要輸入N個數字。
請讓你寫的程式可以判斷T是否曾在這N次輸入中出現過，有的話輸出Found@第幾次輸入的，可能會有多個。
都沒有的話最後請輸出T is not found.。

Input Format
第一行為要找的整數T
第二行為接下來要輸入幾個整數N
第三行為N個整數，中間以空白間隔

Output Format
有找到相符合的話輸出Found@第幾次輸入的(從1起算)，可能會有多個。
都沒有的話最後請輸出T is not found.。

Hint
使用Python解此題須注意
本題的第三行僅有一行輸入，所以第三行只可以有一個input()
input()為字串型態，並以一行為單位，可用split做切割後，再轉數字型態。

'''

T = int(input())

N = int(input())

lst = input().split()

flag = False

for i in range(N):
    if(T==int(lst[i])):
        flag = True
        print("Found@%d"%(i+1))

if(not flag):
    print(T, "is not found.")


