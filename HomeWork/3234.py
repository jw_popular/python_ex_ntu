'''
Task Description
請撰寫一支程式，令使用者輸入二個整數A、B和一個及雙精度浮點數C，進行以下計算：

● A、B和C 的千位數和，平均，標準差
● A、B和C 的百位數和，平均，標準差
● A、B和C 的十位數和，平均，標準差
● A、B和C 的個位數和，平均，標準差
● A、B和C 的和，平均，標準差
如果沒有該位數，請當作0。
The formula of standard deviation.
The formula of standard deviation.
Input format
第一行輸入整數A，0≤A≤1000000
第二行輸入整數B，0≤B≤1000000
第三行輸入雙精度浮點數C，0≤C≤1000000

Output format
第一行A、B和C 的千位數和(整數)，平均(小數點後二位)，標準差(小數點後二位)
第二行A、B和C 的百位數和(整數)，平均(小數點後二位)，標準差(小數點後二位)
第三行A、B和C 的十位數和(整數)，平均(小數點後二位)，標準差(小數點後二位)
第四行A、B和C 的個位數和(整數)，平均(小數點後二位)，標準差(小數點後二位)
第五行A、B和C 的和(小數點後二位)，平均(小數點後二位)，標準差(小數點後二位)
※浮點數均顯示至小數點後2位並4捨5入
每行數字中間以一個空白間隔，結尾無多餘空白
'''

import math
a = int(input())
b = int(input())
c = float(input())

if not (0<=a<=1000000):
    a=0
    print("a is over!")

if not (0<=b<=1000000):
    b=0
    print("b is over!")

if not (0<=c<=1000000):
    c=0
    print("c is over!")


sum=[0, 0, 0, 0, 0]
avg=[0, 0, 0, 0, 0]
std=[0, 0, 0, 0, 0]

#dig=[[1, 2, 3], [4, 5, 6], [7, 8, 9], ['a', 'b', 'c'], [a, b, c]]
dig=[[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [a, b, c]]

for i in range(0, 4):
    dig[i][0]=((a%math.pow(10, 4-i))//math.pow(10, 3-i))
    dig[i][1]=((b%math.pow(10, 4-i))//math.pow(10, 3-i))
    dig[i][2]=((c%math.pow(10, 4-i))//math.pow(10, 3-i))

#print(dig)

for j in range(3):
    for i in range(5):
        sum[i]+=dig[i][j]

for i in range(5):
    avg[i]=sum[i]/3
    ss=0
    for j in range(3):
        ss+=math.pow(dig[i][j]-avg[i], 2)
    std[i]=math.sqrt(ss/3)
    if i!=4:
        print(int(sum[i]), "%.2f"%avg[i], "%.2f"%std[i])
    else:
        print("%.2f"%sum[i], "%.2f"%avg[i], "%.2f"%std[i])

#print(std)
#print(sum)
#print(avg)

