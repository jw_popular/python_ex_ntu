'''
說明
請讓使用者輸入一數列，以append加入list當中(-1結束，list中不含-1)
接著讓使用者輸入一整數d(只有一位數)

將其由小到大排序後印出
在第四個位置插入一整數10後印出(位置由1起算)
印出list中有幾個整數10
刪除數列中個位數是d的元素後，將其由大到小排序後印出
Input Format
一數列

Output Format
順序印出
插入10後印出
有幾個整數10
刪除位數是d的元素後，由大到小排序後印出
'''

lst = []

while(True):
    n = int(input())
    if(n == -1):
        d = int(input())
        break
    else:
        lst.append(n)

lst.sort()
print(lst)

lst.insert(3, 10)
print(lst)

print(lst.count(10))

for i in range(len(lst)-1, 0, -1):
    if(lst[i]%10 == d):
        lst.pop(i)

lst.sort(reverse=True)
print(lst)
