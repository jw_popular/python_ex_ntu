'''
def alternating_sequence(k, arr):
    n = len(arr)
    longest = 0
    for i in range(n-2*k+1):
        cur = [arr[j] for j in range(i, i+k)]
        cur_neg = [arr[j] for j in range(i+k, i+2*k)]
        cur_neg = [x * -1 for x in cur_neg]
        if sorted(cur) == cur and sorted(cur_neg) == cur_neg:
            longest = max(longest, 2*k)
    return longest

k = int(input().strip())
arr = list(map(int, input().strip().split()))[:-1]
print(alternating_sequence(k, arr))
'''


def longest_k_alternating_sequence(k, arr):
    n = len(arr)
    dp = [0 for i in range(n+1)]
    for i in range(1, n+1):
        if arr[i-1] == k:
            for j in range(i-1, max(-1, i-k-1), -1):
                if arr[j] == -k:
                    dp[i] = dp[j] + 1
                    break
    return max(dp)

k = int(input().strip())
arr = list(map(int, input().strip().split()))[:-1]
print(longest_k_alternating_sequence(k, arr))
