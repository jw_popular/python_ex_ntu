

string='''This is a test code 
with the string'''

print(string)
print(repr(string))

string1='This is a test code \
with the string'

print(string1)

string2="This is a test code \
with the string"

print(string2)

string4='''This is a test code with the string'''

print(string4[2:6:1])
print(string4[2:len(string4):2])
print(string4[:6])
print(string4[-1::-1])