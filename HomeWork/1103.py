'''

說明
輸入一個金額1~99999整數, 印中文大寫數字金額
中文大寫1~9: 壹,貳,參,肆,伍,陸,柒,捌,玖
需要印出單位: 萬,仟,佰,拾
不需輸出"零"
最後要印出"元整"
輸入錯誤數值範圍,顯示「out of range」後程式直接結束(請不要印出「」)
輸入輸出格式 (請不要印出輸入提示文字「請輸入金額：」等文字)

Input Format
一整數n

Output Format
結果

'''

num = ["零","壹","貳","參","肆","伍","陸","柒","捌","玖"]

level = ["元整","拾","佰","仟","萬"]

s = ""

n = int(input())

if(1<=n<=99999):
    for i in range(5):
        id = n%(10**(i+1))
        if(i==0):
            if(id!=0):
                s = num[id]+level[0]
            else:
                s = level[0]
        else:
            id = id//(10**i)
            if(id!=0):
                s = num[id]+level[i] + s
    print(s)
else:
    print("out of range")