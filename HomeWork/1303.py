'''
說明
請連續輸入一連串數字(皆為正整數)，直到輸入-1結束，請列出該數列之總合，平均

Input Format
數字1 (正整數)
數字2 (正整數)
...

數字N (正整數)
-1

Output Format
總合
平均(輸出到小數點後1位)

'''

sum = 0

cnt = 0

while(True):
    i = int(input())
    if(i==-1):
        print(sum)
        print("%.1f"%(sum/cnt))
        break
    else:
        sum+=i
        cnt+=1