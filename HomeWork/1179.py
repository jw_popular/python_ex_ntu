'''
說明
令使用者輸入一矩陣的高h與寬w,之後輸入矩陣的內容,之後依序印出此矩陣翻轉與旋轉的結果。

Input Format
第一行為高與寬，各位整數
第二行之後為矩陣的內容，每個數字中間以一空白間隔

Output Format
以下輸出的矩陣內容,每個數字佔5格並靠右對齊,每行結尾後無空白
第一組 正常列印
第二組 右上角與左下角鏡射 (轉換矩陣)(轉置)
第三組 左上角與右下角鏡射
第四組 上下倒並左右相反
第五組 順時鐘旋轉
第六組 逆時鐘旋轉

'''

h, w = input().split()

h = int(h)
w = int(w)

arr = []

for i in range(h):
    tmp = input().split()
    arr.append(tmp.copy())


print(arr)
print(arr.reverse())
        