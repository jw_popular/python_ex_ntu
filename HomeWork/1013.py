'''
說明
製作一個簡單的計算機
功能

連續輸入兩個數字，以空白鍵或換行分開
再輸入＋,－,＊,／ 任一鍵(不需要判斷除以0之情況)
按下Enter後算出結果 (顯示小數點後兩位)

Input Format
數字1
數字2
運算符號

Output Format
輸出結果

'''

a = eval(input())
b = eval(input())

s = input()

if(s=='+'):
    result=a+b

if(s=='-'):
    result=a-b

if(s=='*'):
    result=a*b

if(s=='/'):
    result=a/b

print("%.2f %s %.2f = %.2f"%(a, s, b, result))