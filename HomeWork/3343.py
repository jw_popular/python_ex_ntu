'''
獲取最低位的 "1"
Task Description
將一個十進制整數以 二進制二補數(Two's complement) 表示時
數字是 1 的位數中會存在一個最低位。
何謂最低位？例如某個數長得像 10100 ，所謂的最低位就是 --
從右邊數過來，最先碰到的 1 所在的位數，例如：
將 6 以 4 位二進制二補數表示：
610=01102
則最低位的 1 也就是從右邊數來第 2 位的 1
最後將 0010 以十進制表示可以得 2

禁止使用迴圈
禁止使用 if else
Input Format
一個十進制整數 n

Constraints
−2147483648≤n≤2147483647

Output Format
該十進制整數的二進制二補數表示法中
最低位的 1 的十進制表示法

Sample Input 1
二進制二補數表示法為 000...000110

6
Sample Output 1
2

'''
#print(bin(2147483647))

#print(len(bin(2147483647))-2)
#print(bin(-2147483648))

#print(len(bin(-2147483648))-2)

x = eval(input())

print(x&~(x-1))
