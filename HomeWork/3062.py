'''
說明
請寫一支程式令使用者輸入一正整數n
請利用迴圈及串列的append和pop功能：(C++STL vector/list push_back(),pop_back())
將「資料i (i=1, …, n)」倒序放入串列folder中，例如n=10(10,9,8...1)
然後，再將資料順序取出，例如(1,2,3...10)

Input Format
一整數n

Output Format
反序印出
順序印出
'''

n = int(input())
folder = []

for i in range(1, n+1):
    folder.insert(0, i)

for j in range(n):
    print("data", folder[j])

print("")

for k in range(n):
    print("data", folder[(n-1)-k])
