'''
說明
請輸入一任意位數的數字，並將每一位的數字加總後印出，例如：
n = 1234567
ans = 1+2+3+4+5+6+7 = 28

Input Format
一個正整數

Output Format
一個整數(含換行)
'''

n = input()
sum = 0

#print(type(n))

for i in range(len(n)):
    sum+=int(n[i])
    #print(n[i])

print(sum)
