'''
說明
輸入一數字m
印出2到m之間的質數

Input Format
數字m(0<=m<=100)

Output Format
列出所有2~m之間所有的質數
'''

m = int(input())

if not (0<m<=100):
    m=1

for i in range(1, m+1):
    fct = 0
    for j in range(1,i+1):
        if i%j==0:
            fct+=1
    else: #for loop finished will run here
        if fct==2:
            print(i,'is prime')
