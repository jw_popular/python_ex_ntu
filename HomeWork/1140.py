'''
主題
for loop

說明
 
Flash 閃電俠，世界最快的男人。
有一個傳言，有人說Flash的移動是不連續的，擁有空間跳躍的能力。
爲了要證明Flash的行動軌跡是連續的，專家請來了Flash做實驗。
給予一個整數N 其中0<N<100000,他們要求Flash 跑到N 的位置再折返回來。
過程中都有相機記錄Flash 的位置，請輸出Flash的移動軌跡。
以證明Flash沒有空間跳躍的能力。
例子：
N=5
輸出："123454321\n" （不用空格）

Input Format
輸入一個整數N 0<N<10000.

Output Format
一串數字，Flash的移動軌跡。(沒有空格)

'''

n = int(input())

s = ''

flag = 0

for i in range(1, 2*n):
    if(i<=n):
        flag+=1
    else:
        flag-=1
    s = s + str(flag)

print(s)