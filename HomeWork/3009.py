'''
小練習 說明
請撰寫一程式，可以令使用者輸入一整數n
算出n階乘的結果

Input Format
數字n(0<=n<=12)

Output Format
1乘到 n 的總和
'''

n = int(input())

sum = 1

for i in range(n):
    sum *= i+1

print(sum)