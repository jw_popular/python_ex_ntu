'''
Task Description
請撰寫一程式可以令使用者輸入一介於 100~999之間的數字, 並於每一行之中單獨輸出每一位數字, 共三行,
每一行結尾都要加換行符號，且無空白穿插其中。
Input Format
一行介於 100~999之間的數字
Output Format
有三行輸出，每行之中都只有一個數字。
'''

s = input()

print(s[0])
print(s[1])
print(s[2])
