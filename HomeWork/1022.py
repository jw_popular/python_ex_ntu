'''
說明
請試著輸入一數字n並印出一三角形

Input Format
數字n

Output Format
高為n的*號三角形

'''

n = int(input())

for i in range(1, n+1):
    print("%s%s"%((' ' * (n-i)), ('*' * i)))