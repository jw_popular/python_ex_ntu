'''
說明
撰寫一個程式，先從鍵盤輸入三個整數，然後顯示此三個整數的總和、平均值、乘積、最小值和最大值。螢幕的對話過程應該如下所述。

Input Format
整數1
整數2
整數3

Output Format
總和
平均
積
最小值
最大值
'''

n1 = int(input())
n2 = int(input())
n3 = int(input())

sum = n1+n2+n3

avg = sum/3

prod = n1*n2*n3

if n1>=n2:
    if n1>=n3:
        max=n1
        if n2>=n3:
            min=n3
        else:
            min=n2
    else:
        max=n3
        min=n2
else:
    if n2>=n3:
        max=n2
        if n1>=n3:
            min=n3
        else:
            min=n1
    else:
        max=n3
        min=n1

print("sum is", sum)
print("average is %.2f"%avg)
print("product is", prod)
print("smallest is", min)
print("largest is", max)