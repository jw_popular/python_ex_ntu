'''
說明
冰雪奇緣(Frozen)的ELSA可以瞬間造出樓梯來。
而身為程式設計師的我們，也可以一個指令建出一座樓梯來。
(雖然是虛擬的)

現在請輸入一個整數n(int)，製造出一座層數為n且向右爬升的樓梯吧！

樓梯的地板為"_"，並且每層兩個。
樓梯的支架為"|"。


Input Format
int

Output Format
      __
    __||
  __||||
__||||||
||||||||
...
...
(見Sample Input)

'''

n = int(input())

g = 2
r = 0
a = n*2 - g
l = n + 1

for i in range(1, l):
    print(" " * a + "_" * g + "|" * r)
    a -= 2
    r += 2
    if(i == l-1):
        print("|" * (n*2))
