'''
說明
請試著輸入一數字n並印出一高度為n之聖誕樹
*號個數為奇數，每層之星號遞增二個

Input Format
數字n

Output Format
高為n的*號聖誕樹(每行星號後無空白)

    *
   ***
  *****
 *******
*********

'''

n = int(input())

sum=0

for i in range(1, n+1):
    s = n-i
    r = 1 + 2 * (i-1)
    print("%s%s"%((' '*s),('*'*r)))