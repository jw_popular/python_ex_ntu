'''
說明
已知1900年為鼠年。
並且之後的年份依序為牛、虎、兔、龍、蛇、馬、羊、猴、雞、狗、豬，依照12生肖循環。
如1913年為牛年。


今天有一個python程式，可以回傳年分的生肖，如下：

dic =   {4:'鼠',5:'牛',6:'虎',7:'兔',8:'龍',9:'蛇',10:'馬',11:'羊',0:'猴',1:'雞',2:'狗',3:'豬'}
 
def Year_Checker(n,minguo = False):  #預設西元年(minguo)
    #n必須是整數(int)
    #minguo(代表民國年)
    if minguo == False:
        index = n % 12
        return dic[index]
    elif minguo == True:
        index = (n+1911) % 12
        return dic[index]
先讓使用者先輸入"AD"(西元年)或"Minguo"(民國年)。
再讓使用者輸入一連串年份，直到-1離開，來判斷是什麼生肖。
(python請複製貼上上面的函數來調用）
Input Format
AD 或 Minguo 年份1 年份2 ... -1

Output Format
生肖1 生肖2 ...
'''
dic =   {4:'鼠',5:'牛',6:'虎',7:'兔',8:'龍',9:'蛇',10:'馬',11:'羊',0:'猴',1:'雞',2:'狗',3:'豬'}
 
def Year_Checker(n,minguo = False):  #預設西元年(minguo)
    #n必須是整數(int)
    #minguo(代表民國年)
    if minguo == False:
        index = n % 12
        return dic[index]
    elif minguo == True:
        index = (n+1911) % 12
        return dic[index]

lst = []
result = []

t = input()

if(t == "AD"):
    not_ad=False
elif(t == "Minguo"):
    not_ad=True
else:
    not_ad=False

while(True):
    y = eval(input())
    if(y==-1):
        break
    else:
        lst.append(y)

for i in range(len(lst)):
    print(Year_Checker(lst[i], not_ad))





