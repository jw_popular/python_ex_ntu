'''
說明
阿明覺得自己的電腦放在學校，常常會被朋友亂發文，於是想寫個密碼驗證的程式來保護自己的隱私。

他不想把自己的密碼設定的太複雜，以免自己忘記登不進去，所以密碼僅包含英文字母大小寫與數字。

他也不想像提款機一樣，輸入密碼錯誤超過3次就鎖卡，所以他希望程式可以無限次的嘗試密碼，一直到輸入正確為止。

你可以幫他設計這個程式，讓他的朋友不能再盜他的電腦發文嗎?


Input Format
一開始輸入一個字串，僅包含英文字母和數字，0 < 字串長度 < 100，作為預設的密碼。
接下來每一行的輸入為使用者嘗試的密碼，0 < 嘗試的密碼長度 < 100，僅包含英文字母和數字。
Output Format
若輸入的密碼錯誤，則輸出"Wrong Password!"。

若輸入的密碼正確，則輸出"Correct!"，並且終止程式。
'''

passwd = input()

if(0>=len(passwd) or len(passwd)>=100):
    print("Wrong Password!")
else:
    while(True):
        guess = input()
        if(0<len(guess)<100):
            if(passwd == guess):
                print("Correct!")
                break
            else:
                print("Wrong Password!")
        else:
            print("Wrong Password!")
