'''
說明
請連續輸入一連串數字(皆為正整數)，直到輸入-1結束，請列出該數列之總合，平均、最大值/最小值與其輸入的位置

Input Format
數字1
數字2
... -1

Output Format
總合
平均(輸出到小數點後1位)
最大值
最大值的位置(從一起算)
最小值
最小值的位置(從一起算)

Hint
因重覆次數不可預測，所以建議使用while迴圈

Submit

'''

lst = []

while(True):
    a = int(input())

    if(a==-1):
        break
    else:
        lst.append(a)

sum = 0
max = 0
min = 0
max_index = 0
min_index = -1

for i in range(len(lst)):
    sum += lst[i]
    if(max<=lst[i]):
        max = lst[i]
        max_index=i+1
    if(min>=lst[i] or min_index==-1):
        min = lst[i]
        min_index=i+1

avg = sum / len(lst)

print(sum)
print("%.1f"%avg)
print(max)
print(max_index)
print(min)
print(min_index)
