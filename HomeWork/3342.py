'''
找到落單的數-1
Task Description
在一堆數字中有人落單了，你能找到是誰嗎？
給 5 個整數，這 5 個整數當中只有一個數只出現一次，其餘四個數可能兩兩成對，或者出現四次相同數。

禁止使用迴圈
禁止使用 if else
Input Format
輸入有一行，分別是 5 個整數

Constraints
−20≤ 5 個整數 ≤20

Output Format
輸出有一行，輸出落單的整數

Sample Input 1
有兩組兩兩成對的數字

-2 0 -2 0 4
Sample Output 1
4

'''

lst = input().split()

a = eval(lst[0])
b = eval(lst[1])
c = eval(lst[2])
d = eval(lst[3])
e = eval(lst[4])

if(a^b^c^d^e==e):
    print(e)

if(a^b^c^d^e==d):
    print(d)

if(a^b^c^d^e==c):
    print(c)

if(a^b^c^d^e==b):
    print(b)

if(a^b^c^d^e==a):
    print(a)