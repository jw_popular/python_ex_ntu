'''
說明
請試寫一function 可同時回傳 n! 與 Σ 1~n
並改成即使呼叫時不加入引數也不會錯誤(令n為0)
python: def return2num(n=0)並回傳一tuple, 此tuple中有兩個整數分別為n階與1加到n的結果。
C/C++: void return2num(int n, int *factRst, int *sumRst);

Input Format
一整數n(0<=n<=10)

Output Format
第一行是一整數, 為n!的計算結果
第二行是一整數，為Σ 1~n的計算結果

'''

def return2num(n=0):
    fac = 1
    total = 0
    for i in range(1, n+1):
        fac *= i
        total += i
    return fac, total

n = eval(input())

lst = return2num(n)

print(lst[0])
print(lst[1])




