'''
說明
使用者可以輸入一字元並讓程式判斷此字元是大寫或是小寫英文字母、數字或是其它標點符號。
如果使用者輸入的是小寫英文字母的話，就將其轉大寫後輸出為swap to capital letter X.

Input Format
字元

Output Format
判斷結果
x is a number.
x is a capital letter.
x is a punctuation.

x is a lowercase letter.
swap to capital letter X.

'''

import string

s = input()

if(s.isdigit()):
    print(s,"is a number.")

if(s.isupper()):
    print(s,"is a capital letter.")

if(s.islower()):
    print(s,"is a lowercase letter.")
    print("swap to capital letter %s."%(s.upper()))

if s in string.punctuation:
    print(s,"is a punctuation.")