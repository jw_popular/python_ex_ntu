'''
說明
輸入鍵盤按鍵，讓使用者按下y才執行程式並再輸出run<> 執行完後，可再輸入一次，按下y可重覆執行，其它按鍵則結束程式。

Input Format
y
y
...
其它字元

Output Format
run
run
...

'''

while(True):
    i = input()

    if(i=='y'):
        print("run")
    else:
        break