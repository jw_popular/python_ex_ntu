'''
說明
台北市公館大學某系某科的段考特別難，老師決定幫大家調分，調分規則如下：
原始分數開根號乘以10


使用者輸入分數，並列印出原始分數以及調分後的分數

Input Format
整數(int)或幅點數(float)

Output Format
Original: 分數 (小數點後2位)
Adjusted: 分數(+差幾分(四捨五入至整數)) (小數點後2位)
Hint
1. 引入math模組來計算根號 math.sqrt()

'''

import math
score=eval(input())

new_score=math.sqrt(score)*10

diff = new_score-score
add=diff*100
if add%100 >= 50:
    diff+=1
diff//=1


print("Original: %.2f"%score)
print("Adjusted: %.2f(+%d)"%(new_score, diff))