

import sys
import termios

limit_line = 2
cnt_line = 0

def get_line(prompt=""):
  fd = sys.stdin.fileno()
  old = termios.tcgetattr(fd)
  new = termios.tcgetattr(fd)
  new[3] = new[3] & ~termios.ICANON
  try:
    termios.tcsetattr(fd, termios.TCSADRAIN, new)
    line = input(prompt)
  finally:
    termios.tcsetattr(fd, termios.TCSADRAIN, old)
  return line

str = get_line().strip()
#print('len: {}'.format(len(str)))
print(str)
print(len(str))
'''
str = sys.stdin.readline()

print(type(str))
print(str)
'''

'''
for line in sys.stdin.readline():
    #print(len(line))
    print(line)
    cnt_line += 1
    if(cnt_line==2):
        break
'''
print("Done")

