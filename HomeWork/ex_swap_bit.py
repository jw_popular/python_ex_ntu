'''
交換指定位置的兩個位元位
題目描述：對于一個整型值，在二進制表示中，交換指定位置的兩個位元位的值，
解題代碼：

def swap_bit(x, i, j):
    # 如果第i位和第j位是相同的，則沒必要交換
    if ((x >> i) & 1) != ((x >> j) & 1):
        x ^= ((1 << i) | (1 << j))

    return x


x = 0b0101
i = 0
j = 1
print(bin(swap_bit(x, i, j)))  # 輸出：0b110
總結：這也是一個常用的二進制操作，使用公式
x ^= ((1<<i) | (1<<j))
即可，
^
為二進制的“異或操作”，
'''

x = 0b0101

i = 0
j = 1

print(bin(x))
print(bin((1<<i)|(1<<j)))

x ^= (1<<i)|(1<<j)

print(bin(x))


