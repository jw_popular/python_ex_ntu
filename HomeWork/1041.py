'''
說明
使用者可以輸入字元、字串並輸出測試

Input Format
本題有三行輸入如下：

字元
字串(不含空白)
字串(含空白)
Output Format
本題有三行輸出如下：

字元
字串(不含空白)
字串(含空白)


'''

s1=input()
s2=input()
s3=input()

print(s1)
print(s2)
print(s3)