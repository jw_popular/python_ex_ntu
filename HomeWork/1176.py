'''
說明
小明看到網路流傳一題小六數學題：「如下圖有兩個圓重疊，小圓半徑2公分，大圓半徑3公分，深灰色部分的面積和淺灰色部分的面積大約相差幾平方公分？」


小明非常的困擾，不知該怎麼算，請撰寫一程式讓使用者輸入小圓半徑與大圓半徑，讓程式幫使用者算出淺灰色部分的面積相差幾平方公分吧！
Input Format
小圓半徑(公分 整數) 大圓半徑(公分 整數)

Output Format
相差幾平方公分(印出到小數點後2位)


'''

import math

c1=eval(input())
c2=eval(input())

area1=(c1**2)*math.pi
area2=(c2**2)*math.pi

diff=area1-area2

if diff<0:
    diff*=-1
    
print("%.2f"%diff)