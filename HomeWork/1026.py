'''
說明
阿明覺得自己的電腦放在學校，常常會被朋友亂發文，於是想寫個密碼驗證的程式來保護自己的隱私。

他不想把自己的密碼設定的太複雜，以免自己忘記登不進去，所以密碼僅包含數字。

他也不想向提款機一樣，輸入密碼錯誤超過3次就鎖卡，所以他希望程式可以無限次的嘗試密碼，一直到輸入正確為止。

你可以幫他設計這個程式，讓他的朋友不能再盜他的電腦發文嗎?


Input Format
一開始輸入一個整數，僅包含數字，0 < 數字 < 2000000000，作為預設的密碼。
若不在範圍內則輸出 Wrong Password Setting!並結束程式
若預設密碼符合規範，接下來每一行的輸入為使用者嘗試的密碼，0 < 嘗試的密碼長度 < 100，僅包含數字。
Output Format
若輸入的密碼錯誤，則輸出Wrong Password!。

若輸入的密碼正確，則輸出Correct!，並且終止程式。
'''

passwd = eval(input())

if(0>=passwd or passwd>=2000000000):
    print("Wrong Password Setting!") # notice the output string if correct?
else:
    while(True):
        guess = input()
        if(0<len(guess)<100):
            if(passwd == eval(guess)):
                print("Correct!")
                break
            else:
                print("Wrong Password!")
        else:
            print("Wrong Password!")
