'''
說明
請撰寫一程式，令使用者可以輸入一4位數的數字N，並將每一位的數字加總後印出，例如： 輸入為 1234 答案為 1+2+3+4 => 10

Input Format
一個整數N，範圍為 999<N<10000

Output Format
一個整數(含換行)

'''

s=input()
t=int(s[0])+int(s[1])+int(s[2])+int(s[3])
print(t)