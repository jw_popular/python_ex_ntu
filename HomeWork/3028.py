'''
說明
讓使用者依序輸入三個整數，代表三角形的三個邊長。
請判斷：

判斷是否能成三角形，若可以便輸出「True」;若不行便輸出「False」並結束程式。

判斷是否為直角三角形，若為直角三角形便輸出「Right Triangle」，否則輸出「Non-Right Triangle」。


三角形的判斷方法：「任兩邊長的和大於第三邊」
直角三角形的判斷方法：「兩短邊平方和 = 最長邊平方」(a^2 + b^2 = c^2)
*^代表平方



Input Format
整數
整數
整數

Output Format
True 或 False
Right Triangle 或 Non-Right Triangle (如果上題是True才會顯示這段)
'''

a = eval(input())
b = eval(input())
c = eval(input())

if(a+b>c and a+c>b and b+c>a):
    print(True)
    if(a>=b):
        if(a>=c):
            print("Right Triangle") if (b**2 + c**2 == a**2) else print("Non-Right Triangle")
        else:
            print("Right Triangle") if (a**2 + b**2 == c**2) else print("Non-Right Triangle")
    else:
        if(b>=c):
            print("Right Triangle") if (a**2 + c**2 == b**2) else print("Non-Right Triangle")
        else:
            print("Right Triangle") if (a**2 + b**2 == c**2) else print("Non-Right Triangle")
else:
    print(False)