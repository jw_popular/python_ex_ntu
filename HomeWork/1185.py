'''
說明
請撰寫一程式，印出二位同學的姓名、座號與分數：
一行文字中的每個欄位請用tab間距隔開(\t)
每行文字以換行結尾(\n)
※每列文字中間及最後，除上述指定的間隔與結束符號外，無其他多餘的空白或不可見字元(例如空白,\n,\t)
並請使用者可以自行輸入二位學生的姓名、座號、分數，便會列印如下：

姓名      座號  分數
王小明    20    40
鄭小勳    21    100
Input Format
第一位學生的姓名(字串)
第一位學生的座號(整數)
第一位學生的分數(整數)
第二位學生的姓名(字串)
第二位學生的座號(整數)
第二位學生的分數(整數)

Output Format
共三行，如範例所示，格式依題目敘述輸出
'''


s_name1=input()
site_num1=input()
score1=input()
s_name2=input()
site_num2=input()
score2=input()
print("姓名\t座號\t分數")
print(s_name1,site_num1,score1,sep='\t')
print(s_name2,site_num2,score2,sep='\t')
