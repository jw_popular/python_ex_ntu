'''
說明
猜數字遊戲
讓系統隨機生成一數字介於1到5之間
使用者輸入一數字猜此數字是什麼
猜對的話輸出”猜對了”
猜錯則反之輸出”猜錯了”

但可惜的是系統無法批改亂數題目
因此 此亂數由使用者手動輸入

Input Format
手動輸入之預設答案
猜的數字1-5

Output Format
猜對的話輸出”你猜對了 答案正是 x”
猜錯則反之輸出”猜錯了喔 其實是 x”
'''

result=eval(input())
guess=eval(input())

if(result == guess):
    print("你猜對了 答案正是",result)
else:
    print("猜錯了喔 其實是",result)

