'''
說明
請試著輸入一數字n並印出一高度為n之綾形
*號個數為奇數，每層之星號遞增/減二個

Input Format
數字n(需為奇數)

Output Format
高為n的*號綾形(星號後無空白)

  *
 ***
*****
 ***
  *
  

'''


n = int(input())

x = n//2+1

star = 1

for i in range(1, n+1):
	if(i<x):
		print("%s%s"%((' '*(x-i)),('*'*star)))
		star+=2
	elif(i==x):
		print("%s"%('*' * n))
		star-=2
	else:
		print("%s%s"%((' '*(i-x)),('*'*star)))
		star-=2


