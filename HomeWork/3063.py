'''
說明
請讓使用者輸入一數列，以append加入list當中(-1結束，list中不含-1)

將其由小到大排序後印出
在第四個位置插入一整數10後印出(位置由1起算)
印出list中有幾個整數10
將其由大到小排序後印出
Input Format
一數列

Output Format
排序印出(由小到大)
插入10後印出
有幾個整數10
由大到小排序後印出
'''

lst = []

while(True):
    n = int(input())
    if(n == -1):
        break
    else:
        lst.append(n)

lst.sort()
print(lst)

lst.insert(3, 10)
print(lst)

print(lst.count(10))

lst.sort(reverse=True)
print(lst)
