'''
Problem description
Write a program to find the alternating sequence from the input that has the maximum length. 

A k-alternating sequence is a sequence of alternating k positive integers and 

k negative integers that starts and ends both with k positive integers, and 
contains at least one set of k negative integers in between. For example, 
when k is 2 then (2,1,-3,-2,7,100) is a 2-alternating sequence, 
but none of (1,1,-23,4), (1,2), (1,1,3,-1,1,1) is.

Let us consider the following example. 

Now given an input sequence (-3,4,6,-5,-3,10,7,-1,-1,3,-7,3), 
then 

(4,6,-5,-3,10,7) is the longest 2-alternating sequence and 

(3,-7,3) is the longest 1-alternating sequence.

Now given k and a sequence of n integers that ends in 0, 
please find the length of the longest k-alternating sequence. 
If there are no k-alternating sequences in the input please report 0.

Technical Specification and constraints
30 pt. n is between 1 and 20, and k is 1.
60 pt. n is between 1 and 100000 and k is between 1 and n.
10 pt. n is positive, and k is between 1 and n.
Input Format
There are two lines in the input. 
The first line has the integer k. 
The second line has the sequence integers end with 0.

Output Format
Only one line print the length of the longest k-alternating sequence.

Sample Input 1
1
1 -1 1 1 -1 1 -1 1 0
Sample Output
5

Hint
We observe that since the sequence ends in 0, 

there will be only segments of positive and negative numbers before that. 

Therefore you only need to remember whether you are in the middle of 
a positive or negative segment, and how many numbers you have seen in this segment. 
Also you need to remember whether you are in the middle of an alternating sequence or not. 
Now whenever you switch from a positive segment to a negative segment, 
or from a negative to a positive segment, you need to update these information, 
and the length of the longest sequence you have seen so far. 
After processing through the sequence you will know the answer.

Explain for Sample 1
k = 1
(1 -1 1) 1 -1 1 -1 1 0
length = 3
1 -1 1 (1 -1 1 -1 1) 0
length = 5
Explain for Sample 2
k = 2
-3 (4 6 -5 -3 10 7) -1 -1 3 -7 3 0
length = 6
'''
import os
import sys
import readline

if (os.name=="nt"):
    import msvcrt
else:
    import tty
    import termios


'''
def get_line(prompt=""):
  fd = sys.stdin.fileno()
  old = termios.tcgetattr(fd)
  new = termios.tcgetattr(fd)
  new[3] = new[3] & ~termios.ICANON
  try:
    termios.tcsetattr(fd, termios.TCSADRAIN, new)
    line = input(prompt)
  finally:
    termios.tcsetattr(fd, termios.TCSADRAIN, old)
  return line

'''


k = int(input())

#s = input()[1 * 1024 * 1024]
#s = get_line()
s = readline()[1 * 1024 * 1024]

print(len(s))

lst = s.split()

#print(len(lst))
#n = lst.index("0")
#lst.remove("0")
n = 0
for i in range(len(lst)):
    if(lst[i]=='0'):
        n = i
        lst = lst[:i]
        break

#print(n) 
#print(lst)

cnt = 0
ret = 0
result = [0, 0]

def check_k(k: int, lst: list) -> int:
    ret_val = 0
    sublst = []
    num = len(lst)
    #print("k =", k, lst, "len =", num)
    for i in range(0, num, k):
        tmp_lst = lst[i:i+k]
        if((len(tmp_lst))==k):
            sublst.append(tmp_lst)
    
    sub_num = len(sublst)
    sub_num = sub_num - (sub_num%2)
    #print(sublst,"sub_num =", sub_num)
    if (sub_num<2):
        return 0

    for i in range(0, sub_num, 2):
        filter_sum = [0, 0, 0, 0]
        #print(sublst[i], "", len(sublst[i]), ";", sublst[i+1], "", len(sublst[i+1]))
        for j in range(k):
            filter_sum[0] += abs(int(sublst[i][j]))
            filter_sum[1] += (int(sublst[i][j]) * (-1))
            filter_sum[2] += abs(int(sublst[i+1][j]))
            filter_sum[3] += (int(sublst[i+1][j]) * (1))

        #print(filter_sum)
        if((filter_sum[0]==filter_sum[1]) and (filter_sum[2]==filter_sum[3])):
            ret_val += 2 * k
        else:
            return ret_val
    return ret_val


for i in range(n-(k-1)):
    window=lst[i:(i+k)]
    #print(window)
    sum1 = 0
    sum2 = 0
    sum3 = 0
    sum4 = 0
    for j in range(len(window)):
        sum1 += abs(int(window[j]))
        sum2 += (int(window[j]) * (1))
    #print("---->", sum1, sum2)
    if(sum1 == sum2):
        #print("Start :", window)
        ret = check_k(k, lst[i+k:])
        #print("ret=", ret)
        #print(type(ret), type(k))
        if(ret!=0):
            ret = ret + k
            if(result[1]<ret):
                result[0]=i
                result[1]=ret
        

#print("result =", result)    
print(result[1])


