'''
說明

請算出此梯型面積
上底：10cm
下底：15cm
高：7cm

Input Format
沒有輸入

Output Format
輸出梯型面積到小數點後1位
(Python的同學先不用管，題目設計自然只會輸出到小數點後1位)
'''

top=10
bottom=15
high=7
s=(top+bottom)*high/2
print(s)