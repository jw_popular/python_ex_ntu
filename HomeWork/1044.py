'''
說明
使用者可以輸入一字元c，並讓程式判斷此字元是大寫或是小寫英文字母、數字或是其它標點符號。

Input Format
字元c

Output Format
判斷結果:
x is a number.
x is a capital letter.
x is a lowercase letter.
x is a punctuation.
'''
import string

s = input()

if(s.isdigit()):
    print(s,"is a number.")

if(s.isupper()):
    print(s,"is a capital letter.")

if(s.islower()):
    print(s,"is a lowercase letter.")

if s in string.punctuation:
    print(s,"is a punctuation.")
