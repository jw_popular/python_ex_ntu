'''
說明
連續輸入一串數字，數字的範圍在0-9之間，
請判斷這連續數字當中，有幾個9是緊接在1後面的。

例如：
1
9
這個9就算是緊接在1後面。
而：
1
3
9
這個9中間被3隔斷了，不算是緊接在1後面。



Input Format
int (0-9之間)
...
...
...
q

Output Format
int (有幾個9是接在1後面)

'''
pre = 0
cnt = 0

while(True):
    n = input()

    if(n.isdecimal()):
        val = int(n)
        if(pre!=0):
            if(val==9 and pre==1):
                cnt+=1
        pre = val
    else:
        break

print(cnt)