'''
說明 3091.py
試寫一個名為student的類別
其中屬性包含:
name, gender, grades
方法(函數)包含:
__init__(name,gender)：在建立實體實傳入 姓名及性別
avg(): 回傳grades list的平均值，回傳一浮點數
add(grade): 新增成績到grades list中，無回傳值
fcount(): 回傳不及格(<60)的總數，為一整數
show(): 可以將學生資料依格式列印出來，無回傳值
以上不要忘記傳入self

說明
延續類別練習 I

試寫一個名為student的類別
其中屬性包含:
name, gender, grades
方法(函數)包含:
avg: 回傳grades list的平均值
add(grade): 新增成績到grades list中
fcount: 回傳不及格(<60)的總數
show: 可以將學生資料依本題格式列印出來 在建立實體實傳入 姓名及性別

將下方程式碼加入類別中

s1 = student("Tom","M")
s2 = student("Jane","F")
s3 = student("John","M")
s4 = student("Ann","F")
s5 = student("Peter","M")
s1.add(80)
s1.add(90)
s1.add(55)
s1.add(77)
s1.add(40)
s2.add(58)
s2.add(87)
s3.add(100)
s3.add(80)
s4.add(40)
s4.add(55)
s5.add(60)
s5.add(60)
 
top_student = top(s1,s2,s3,s4,s5)


分別將每個學生的成績 平均、不及格的的數目印出
於類別外寫一個top的函數: def top(*students): -> student
傳入值為學生物件的序列，以不定個參數的型式傳入
將平均分數最高的學生回傳,回傳值為自訂之student類別型態的實體

.......
 
Name: Peter
Gender: M
Grades: [60, 60]
Avg: 60.0
Fail Number: 0
 
Top Student:
Name: John
Gender: M
Grades: [100, 80]
Avg: 90.0
Fail Number: 0


Hint
※ 特別注意Top Student的最後一行也要換行
'''


class student:
    def __init__(self, n, gend) -> None:
        self.name=n
        self.gender=gend
        self.grades = []
    def avg(self):
        return sum(self.grades)/len(self.grades)
    def add(self, val):
        self.grades.append(val)
    def fcount(self):
        ret = 0
        for i in range(len(self.grades)):
            if self.grades[i]<60:
                ret+=1
        return ret
    def show(self):
        print("Name:",self.name)
        print("Gender:", self.gender)
        print("Grades:", self.grades)
        print("Avg: %.1f"%self.avg())
        print("Fail Number:", self.fcount())
        print()

def top(*students) -> student:
    t_op = 0
    top_s = student
    #print(type(students))
    
    for s in students:
        if t_op <= s.avg():
           top_s = s
           t_op = s.avg()
    return top_s


        


s1 = student("Tom","M")
s2 = student("Jane","F")
s3 = student("John","M")
s4 = student("Ann","F")
s5 = student("Peter","M")
s1.add(80)
s1.add(90)
s1.add(55)
s1.add(77)
s1.add(40)
s2.add(58)
s2.add(87)
s3.add(100)
s3.add(80)
s4.add(40)
s4.add(55)
s5.add(60)
s5.add(60)

s1.show()
s2.show()
s3.show()
s4.show()
s5.show()

top_student = top(s1,s2,s3,s4,s5)
print("Top Student:")
top_student.show()