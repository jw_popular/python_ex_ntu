'''
小練習 說明
請撰寫一程式，令使用者輸入一整數半徑r
請列出圓形之圓周率，半徑，圓周長及圓面積

Input Format
半徑(整數)

Output Format
圓周率
半徑(整數)
圓周長
圓面積

Hint
import math
math.pi
本題輸出專為python 所設計，若C與C++想達成一樣的結果
要設定 PI 為 3.141592653589793之外
要可以自動判別小數點的位數(用cout最簡單)
而printf的話就比較麻煩了


'''

import math

a=eval(input())
print('圓周率', math.pi)
print('半徑', a)
print('圓周長', a*2*math.pi)
print('圓面積', a**2*math.pi)

