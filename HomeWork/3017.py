'''
說明
請連續輸入一連串數字(皆為正整數)，直到輸入-1結束，請列出該數列之總合，平均與最大值與其輸入的位置

Input Format
數字1 (正整數)
數字2 (正整數)
...

數字N (正整數)
-1

Output Format
總合
平均(輸出到小數點後1位)
最大值
最大值的位置(從一起算)

'''

sum = 0

cnt = 0

max = 0

index = 0

while(True):
    i = int(input())
    if(i==-1):
        print(sum)
        print("%.1f"%(sum/cnt))
        print(max)
        print(index)
        
        break
    else:
        sum+=i
        cnt+=1
        if(i>=max):
            max=i
            index=cnt


