'''
說明
請使用set集合功能來完成以下問題: 米花市帝丹小學一年级B班正舉辦期中考試,
數學及格的有：柯南、灰原、步美、美環、光彦，
英文及格的有：柯南、灰原、丸尾、野口、步美。
請分別列出「數學及格且英文不及格的同學名單」、「數學不及格且英文及格的同學名單」和「兩者皆及格」名單。
最後將結果轉換成為list並排序後輸出

Input Format
無

Output Format
數學及格但英文不及格
英文及格但數學不及格
英文及格且數學及格

'''

math_set = {"柯南", "灰原", "步美", "美環", "光彦"}
eng_set = {"柯南", "灰原", "丸尾", "野口", "步美"}

set1 = math_set.difference(eng_set)
set2 = eng_set.difference(math_set)

#set2 = math_set.union(eng_set)
#setA = math_set.difference(set1)
#setB = eng_set.difference(set2)


set3 = math_set.intersection(eng_set)

lst1 = list(set1)
lst1.sort()
print(lst1)

lst2 = list(set2)
lst2.sort()
print(lst2)

lst3 = list(set3)
lst3.sort()
print(lst3)