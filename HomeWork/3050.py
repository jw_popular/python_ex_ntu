'''
說明
請試著輸入一數字n並印出一高度為n之
由遞增數字所組成之三角形

Input Format
數字n

Output Format
高為n的數字三角形(每個數字後含一空白)

1
2 3
4 5 6
7 8 9 10
11 12 13 14 15

'''

n = int(input())

sum=0

for i in range(1, n+1):
    for j in range(1, i+1):
        sum+=1
        print(sum, end=' ')
    print()