'''
說明
在平面座標X、Y上，A、B、C三點的位置分別為(a,b)、(c,d)、(e,f)。
若要計算A與B兩點距離，其公式為：




而這三個點的直線可以圍成一個三角形
請利用海龍公式(Heron's formula)計算出三角形的面積。
公式如下：




Input Format
int/float int/float
int/float int/float
int/float int/float
(輸入為三行，每行各兩個整數或浮點數，且中間以空格隔開）

Output Format
三角形面積 (四捨五入至小數點後二位，就算是整數也要至小數點第二位）
(請在列印出三角形面積時再四捨五入，不要再算三邊的長度時就四捨五入)
'''

import math

a, b=[float(x) for x in input().split()]
c, d=[float(x) for x in input().split()]
e, f=[float(x) for x in input().split()]

lst=[a, b, c, d, e, f]

dist=[0, 0, 0]

dist[0]=(c-a)**2+(d-b)**2
dist[1]=(e-c)**2+(f-d)**2
dist[2]=(a-e)**2+(b-f)**2

s=0
if dist[0]>0 and dist[1]>0 and dist[2]>0:
    for i in range(3):
        dist[i]=math.sqrt(dist[i])
        s+=dist[i]
    s/=2
    if s<=0:
        area=0
    elif s-dist[0]<=0 or s-dist[1]<=0 or s-dist[2]<=0:
        area=0
    else:
        area=math.sqrt(s*(s-dist[0])*(s-dist[1])*(s-dist[2]))
else:
    area=0

print("%.2f"%area)