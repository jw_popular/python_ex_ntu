'''
小練習 說明
請撰寫一程式，可以令使用者輸入一大於0的整數n
算出1加到n的結果

Input Format
數字n

Output Format
1加到 n 的總和

Hint
sum請務必初始化
'''

n = int(input())

total = 0
for i in range(n+1):
    total+=i

print(total)