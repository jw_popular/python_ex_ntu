'''
說明
寫個程式，判斷一個人的成績是否及格 (大學部及格分數為60分，研究所為70分)
令使用者輸入他的學藉及成績用數字表示學藉：

(1)大學部(2)研究所，並判斷其成績是否及格

Input Format
學藉：(1)大學部 (2)研究所
成績 (0<=score<=100)

Output Format
pass或fail
'''

level=eval(input())

score=eval(input())

if(level==1):
    if(score>=60):
        print("pass")
    else:
        print("fail")
else:
    if(score>=70):
        print("pass")
    else:
        print("fail")