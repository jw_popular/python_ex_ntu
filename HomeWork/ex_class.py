class Pokemon:
    pct = 100

    def __init__(self, name, lv) -> None:
        self.Name = name
        self.Lv = lv
    def Attack(self):
        pass
    def Defence(self):
        pass
    def Cure(self):
        pass
    def ShowInfo(self):
        print("I am pokemon.")
        print(self.Name)
        print(self.Lv)

#help(Pokemon)

print(dir(Pokemon))


pk = Pokemon("PiCa", 100)
pk2 = Pokemon("PiCa2", 200)

print(type(Pokemon))

print(type(pk))

print(pk.pct)

pk.ShowInfo()
pk2.ShowInfo()

print(pk.Name)
print(pk2.Lv)