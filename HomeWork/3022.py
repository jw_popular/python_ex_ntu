'''
說明
現有一數列如下：1,2,3,4,5,6,7,8,9,10
請將索引為：

1.奇數的數字加總
2.偶數的數字加總
3.將其所有的數字加總
Input Format
字串 (一串數列組成的字串)

Output Format
整數 (奇數的數字加總)
整數 (偶數的數字加總)
整數 (所有的數字加總)

Hint
利用.split()

'''

lst = input().split(sep=',')

add_sum = 0
even_sum = 0
sum = 0

for i in range(len(lst)):
    n = int(lst[i])
    sum += n
    if(n%2==0):
        even_sum += n
    else:
        add_sum += n

print(add_sum)
print(even_sum)
print(sum)