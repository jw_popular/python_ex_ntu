'''
小練習 說明
獎學金額判斷程式
請撰寫一支程式可以判斷使用者可以得到多少獎學金
令使用者輸入一整數score為成績(0≤score≤100)

若使用者輸入的成績分數達95分以上
得獎金 2000 元
若是90分以上到94分
得獎金 1000 元
若是80分以上到89分
得獎金 500 元
若不到80分則無獎金
得獎金 0 元

Input Format
成績

Output Format
獎金
'''

score=eval(input())

if(score>=95):
    bonus=2000
elif(90<=score<95):
    bonus=1000
elif(80<=score<90):
    bonus=500
else:
    bonus=0

print("獎金",bonus,"元")
