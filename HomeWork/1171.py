'''
Task Description
請撰寫一程式，令使用者輸入一數字N，接著連續輸入N個數字
請找出，這些數字的最大值、最小值及其位置。
Input format
第一行輸入整數N，0≤N≤1000000
接下來1~N行，輸入均為整數，0≤1~N≤1000000

Output format
最大值(整數) 第一個最大值的位置(整數) 最後一個最大值的位置(整數，如果有多個的話才輸出，沒有的話不用)
第二大值(整數) 第一個第二值的位置(整數) 最後一個第二大值的位置(整數，如果有多個的話才輸出，沒有的話不用)
最小值(整數) 第一個最小值的位置(整數) 最後一個最小值的位置(整數，如果有多個的話才輸出，沒有的話不用)
第二小值(整數) 第一個第二小值的位置(整數) 最後一個第二小值的位置(整數，如果有多個的話才輸出，沒有的話不用)
11
1
8
6
2
5
4
6
3
7
1
2
8 2
7 9
1 1 10
2 4 11

以第十筆為例，第二大是8，第二小也是8，然後一樣要輸出4行
2
1
1

5
8
8
8
8
8

20
22
55
66
77
55
77
33
55
33
55
22
44
77
21
77
55
77
33
77
33

'''

n = int(input())


max_pos = [[-1, 0, 0], [-1, 0, 0]]

min_pos = [[1000001, 0, 0], [1000001, 0, 0]]

def pStatus():
    #print("------Begin------")
    if(max_pos[0][0]!=-1):
        if(max_pos[0][2]==0):
            print(max_pos[0][0], max_pos[0][1])
        else:
            print(max_pos[0][0], max_pos[0][1], max_pos[0][2])

    if(max_pos[1][0]!=-1):
        if(max_pos[1][2]==0):
            print(max_pos[1][0], max_pos[1][1])
        else:
            print(max_pos[1][0], max_pos[1][1], max_pos[1][2])

    if(min_pos[0][0]!=1000001):
        if(min_pos[0][2]==0):
            print(min_pos[0][0], min_pos[0][1])
        else:
            print(min_pos[0][0], min_pos[0][1], min_pos[0][2])

    if(min_pos[1][0]!=1000001):
        if(min_pos[1][2]==0):
            print(min_pos[1][0], min_pos[1][1])
        else:
            print(min_pos[1][0], min_pos[1][1], min_pos[1][2])
    #print("------End------")
    return


for i in range(n):
    val = int(input())
#----------------------------------------------------------------#

    if(max_pos[0][0]==-1 and min_pos[0][0]==1000001):
        max_pos[0][0]=val
        max_pos[0][1]=i+1
        min_pos[0][0]=val
        min_pos[0][1]=i+1
        continue

    if(val == max_pos[0][0]):
        max_pos[0][2]=i+1
        if(max_pos[1][0]==-1):
            max_pos[1][0]=val
            max_pos[1][1]=max_pos[0][1]

    if(val == max_pos[1][0]):
        max_pos[1][2]=i+1

    if(val == min_pos[0][0]):
        min_pos[0][2]=i+1
        if(min_pos[1][0]==1000001):
            min_pos[1][0]=val
            min_pos[1][1]=min_pos[0][1]

    if(val == min_pos[1][0]):
        min_pos[1][2]=i+1

    if(val > max_pos[0][0]):
        if(max_pos[0][0] > max_pos[1][0]):
            max_pos[1][0]=max_pos[0][0]
            max_pos[1][1]=max_pos[0][1]
            max_pos[1][2]=max_pos[0][2]
        max_pos[0][0]=val
        max_pos[0][1]=i+1
        max_pos[0][2]=0
    elif(val < max_pos[0][0] and val > max_pos[1][0]):
        max_pos[1][0]=val
        max_pos[1][1]=i+1
        max_pos[1][2]=0

    if(val < min_pos[0][0]):
        if(min_pos[0][0] < min_pos[1][0]):
            min_pos[1][0]=min_pos[0][0]
            min_pos[1][1]=min_pos[0][1]
            min_pos[1][2]=min_pos[0][2]
        min_pos[0][0]=val
        min_pos[0][1]=i+1
        min_pos[0][2]=0
    elif(val > min_pos[0][0] and val < min_pos[1][0]):
        min_pos[1][0]=val
        min_pos[1][1]=i+1
        min_pos[1][2]=0

pStatus()
#----------------------------------------------------------------#
