'''
波西傑克森的閱讀障礙
說明
波賽頓之子波西傑克森擁有能夠操控水體的能力，在眾神中是能力甚強的混血孩子之一。
然而在進入混血營之前，他的童年非常不順遂。 
自小被診斷出閱讀障礙以及過動症，導致他進入混血營之前一直有學習困難，是老師眼中的問題學生。 
深深相信他潛力的老師aka你，嘗試著理解他閱讀時的思路，有了以下的發現：

所有他看到的數字（二進位制）都會被左右顛倒。

舉例而言：
假設一個十進位數字13(在二進位中是00000000000000000000000000001101)，
其反轉後的結果為10110000000000000000000000000000(在十進位中是2952790016)
因此數字13會被波西傑克森看成2952790016。

保證題目給定的十進位數字皆為可用32位元表示的非負整數。 請輸出波西傑克森解讀到的數字轉換為十進位非負整數後的結果。

PS. 禁止使用 if while for

##解析ppt: https://docs.google.com/presentation/d/1FFLm93ufIgWWsKLj00UxP4l5EXHMc6PsQ-tkfgUbYcE/edit?usp=sharing

Input Format
輸入為一個十進位非負整數N。保證 N可以被表示為 32 位元的二進位數(不足32位請補0)。

Output Format
請輸出一個十進位非負整數，代表波西傑克森解讀到的數字轉換後的結果。


'''

a = 1234567890

s_bin = bin(a)

print(s_bin)

print(len(s_bin))

s1_bin = '0b11111111111111111111111111111111'
s2_bin = '0b01001001100101100000001011010010'


print(int(s1_bin, 2))

print(len(s1_bin))


print(int(s2_bin, 2))

a = 777
a1 = '{0:05d}'.format(a)
print(a1)

print(type(a1))

b = '777'
b1 = '{:5s}'.format(b)
print(b1)

print(type(b1))

n1 = '0b1010'
n = int(n1, 2)

print(n1, " = ", n)

n2 = (n & 10) >> 1
print(n2, " = ", bin(n2))
n3 = (n & 5) << 1
print(n3, " = ", bin(n3))
n = n2 | n3

print(n, " = ", bin(n))

n5 = (n & 12) >> 2
print(n5, " = ", bin(n5))

n6 = (n & 3) << 2
print(n6, " = ", bin(n6))

n = n5 | n6

print(n, " = ", bin(n))





