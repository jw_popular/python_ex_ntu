'''
說明
令使用者輸入一介於1~100的數字
依台大的等第制換算等第績分(GPA)與等第成績

4.3     90-100      A+
4.0     85-89       A
3.7     80-84       A-
3.3     77-79       B+
3.0     73-76       B
2.7     70-72       B-
2.3     67-69       C+
2.0     63-66       C
1.7     60-62       C-
0       0-59        F


Input Format
整數(成績)

Output Format
浮點數(等第績分(GPA)：4.3、4.0、3.7、3.3......)
字串(等第成績:A+、A、A-、B+......)

Hint
1. 0不是浮點數
2. 如果輸出GPA為4分必須輸出為4.0

'''

tuple1 = (4.3, 4.0, 3.7, 3.3, 3.0, 2.7, 2.3, 2.0, 1.7, 0)
tuple2 = ("A+", "A", "A-", "B+", "B", "B-", "C+", "C", "C-", "F")

score = int(input())

if(90<=score<=100):
    i=0
elif(85<=score<=89):
    i=1
elif(80<=score<=84):
    i=2
elif(77<=score<=79):
    i=3
elif(73<=score<=76):
    i=4
elif(70<=score<=72):
    i=5
elif(67<=score<=69):
    i=6
elif(63<=score<=66):
    i=7
elif(60<=score<=62):
    i=8
else:
    i=9

if(i==9):
    print("%d"%tuple1[i])
    print("%s"%tuple2[i])
else:
    print("%.1f"%tuple1[i])
    print("%s"%tuple2[i])