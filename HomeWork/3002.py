'''
說明

請撰寫一程式令使用者可依序輸入上底、下底、高
並算出梯型面積，且輸出計算結果

Input Format
輸入有3行，分別如下：

上底(整數)
下底(整數)
高(整數)
Output Format
輸出僅有一行為梯型面積到小數點後1位
(Python的同學先不用管，題目設計自然只會輸出到小數點後1位)
'''

top=int(input())
bottom=int(input())
high=int(input())
s=(top+bottom)*high/2
print(s)