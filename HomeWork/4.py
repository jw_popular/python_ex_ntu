'''
Task Description
Write a program to read three integers a, b, and c, then print their sum.

Input Format
There are three lines in the input. The first line has the integer a, 
the second line has the integer b, and the third line has the integer c.

Output Format
There is one lines in the output. The line has the sum of a, b, and c.
'''
suml = 0
for i in range(3):
    val = int(input())
    suml += val
print(suml)