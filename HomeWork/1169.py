'''
Task Description
請撰寫一程式，令使用者輸入一數字N，接著連續輸入N個數字
請找出，這些數字的最大值、最小值及其位置。
Input format
第一行輸入整數N，0≤N≤1000000
接下來1~N行，輸入均為整數，0≤1~N≤1000000

Output format
最大值(整數) 最大值的位置(整數)
最小值(整數) 最小值的位置(整數)
※如果有多個最大/小值，請輸出第一次出現的位置

'''

n = int(input())

max = -1
max_pos = 0
min = 1000001
min_pos = 0

for i in range(n):
    val = int(input())
    if(val > max):
        max=val
        max_pos=i+1

    if(val < min):
        min = val
        min_pos=i+1

print(max, max_pos)
print(min, min_pos)
