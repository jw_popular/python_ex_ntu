'''
說明
請利用迴圈及串列的append和pop功能：(C++STL vector/list push_back(),pop_back())
輸入一數字n後
輸入n筆「字串」資料，並依序放入串列中， 然後，再將資料倒序取出

Input Format
一整數n
第1筆字串
第2筆字串
....
第n筆字串

Output Format
加上data字樣後反序印出
'''

n = int(input())
folder = []

for i in range(n):
    val=eval(input())
    folder.insert(0, val)

for j in range(n):
    print("data", folder[j])