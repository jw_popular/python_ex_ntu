'''
小練習 說明
請建立一dictionary 名為 short 其kay為
NTU, TW, 112, CSIE, Train
所對應的value為
台灣大學, 台灣, 台大ip, 資工系, 訓練班
本題需要你的程式可以令使用者可自由輸入列印指令:
print("%(NTU)s"%short)

Input Format
字典short的列印指令

Output Format
印出對應的字串

Hint
可用`eval(input())` 來執行使用者所輸入的指令

'''

short = {"NTU":"台灣大學", "TW":"台灣", "112":"台大ip", "CSIE":"資工系", "Train":"訓練班"}

eval(input())