'''
說明
請撰寫一程式，令使用者可以輸入任意兩個整數，並印出兩者相加的結果

Input Format
數字1(整數)
數字2(整數)

Output Format
輸出兩數(整數)相加結果

Sample Input
5
2
Sample Output
7
Hint
注意是可以讓使用者輸入任意的2個整數，而非固定的2和5
請勿輸出其他多餘的文字！
'''

x=input()
y=input()
s=int(x)+int(y)
print(s)