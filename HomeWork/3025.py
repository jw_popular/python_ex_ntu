'''
說明
小明常年住在赤道國家（沒有四季），今年因爲求學而來到了臺灣。
小明卻非常不熟悉這裏的氣候，因此都要查查看到底現在是什麽季節。
所以小明會提問月份，請幫他判別到底那個時候是什麽季節。
已知
3~5: Spring
6~8: Summer
9~11: Autumn
1、2、12: Winter
如果月份不存在的話就輸出， "Month doesn't exist!"

Input Format
輸入1個1~12數值，如果超出此範圍則印出"Month doesn't exist!"(沒有空格)
否則利用switch印出相對應的季節:
3~5: Spring
6~8: Summer
9~11: Autumn
1、2、12: Winter

Output Format
刻月份的季節

'''

mon = int(input())

if(mon<1 or mon>12):
    print("Month doesn't exist!")
else:
    if(3<=mon<=5):
        print("Spring")
    elif(6<=mon<=8):
        print("Summer")
    elif(9<=mon<=11):
        print("Autumn")
    else:
        print("Winter")

