'''
正數和0的二補數就是該數字本身再補上最高位元0。負數的二補數則是將其對應正數按位元取反再加1。


n 的 2's complement n' = ~n + 1

=> ~n = n'-1
=> ~(~n) = ~(n'-1)
=> n = ~(n'-1)

'''
number = 0    

print("number= ", number, " <=> ", "{0:08b}".format(number))
print("number*(-1)= ", number*(-1), " <=> ", "{0:08b}".format(number*(-1)))
print("~number+1= ", ~number+1, " <=> ", "{0:08b}".format(~number+1))
print("~(number*(-1))+1= ", ~(number*(-1))+1, " <=> ", "{0:08b}".format(~(number*(-1))+1))

print("~(number-1)= ", ~(number-1), " <=> ", "{0:08b}".format(~(number-1)))
print("~(~number)= ", ~(~number), " <=> ", "{0:08b}".format(~(~number)))

print("number & ~(number-1)= ", number & ~(number-1), " <=> ", "{0:08b}".format(number & ~(number-1)))

print("--------------------------")

print(int('0b11111111111111111111111111111111',2))
print(int('0b10000000000000000000000000000000',2))
print(int(bin(int('0b10000000000000000000000000000000',2)*(-1)),2))
print(int('0b01111111111111111111111111111111',2))
print(int('-0b11111111111111111111111111111111',2))
print(int('-0b10000000000000000000000000000000',2))
print(int('-0b01111111111111111111111111111111',2))


print("--------------------------")

n = "{0:08b}".format(number)

print(n, ": ", type(n))

#binary_number = int("{0:08b}".format(number))

binary_number = int(n)

print(binary_number, "==>",bin(binary_number))

flipped_binary_number = ~ binary_number

print(flipped_binary_number, "==>",bin(flipped_binary_number))

flipped_binary_number = flipped_binary_number + 1

print(flipped_binary_number, "==>",bin(flipped_binary_number))

str_twos_complement = str(flipped_binary_number)

print(str_twos_complement)

twos_complement = int(str_twos_complement, 2)

print(twos_complement)

