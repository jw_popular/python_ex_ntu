'''
說明
打(Dozen)是日常生活中常用的單字，為英制單位代表12個。
如：1打雞蛋等同於12顆雞蛋。
會被使用是因為生活中便於分配的結果，12可以被1,2,3,4,6整除；而10只能被1,2,5整除。

今天令使用者輸入一個整數(int)，我們將數字轉換成打(Dozen)。
例如：25 = 2 dozen and 1

Input Format
整數(int)

Output Format
若是無法整除有餘數的輸出：? dozen and ?
若是可以整除無餘數的輸出：? dozen
'''

n=int(input())

dozen=n//12
mod=n%12

print("%d dozen"%dozen,end='')

if mod>0:
    print(" and %d"%mod)
else:
    print('')
