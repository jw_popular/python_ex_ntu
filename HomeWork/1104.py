'''
說明
輸入一個金額1~9999999整數, 印出英文數字金額
英文1~9: one two three four five six seven eight nine
英文10~90: ten twenty ... ninety 需要印出單位: hundred thousand million
最後要印出"dollar(s)" 
輸入錯誤數值範圍,顯示錯誤後程式直接結束

Input Format
一整數n

Output Format
結果

Hint:
要處理 ten ~ twenty 之間的數字
dollar 需要處理後面的 s

'''

num = ["zero","one","two","three","four","five","six","seven","eight","nine"]

level = ["dollar","ten","twenty","thirty","fourty","fifty","sixty","seventy","eighty","ninety"]

level_1 = ["","eleven", "twelve", "thirteen", "fourteen", "fifteen", "sizteen", "seventeen", "eighteen", "nineteen"]

level_2 = ["hundred","thousand","million"]

s = ""

n = int(input())

deal_l1 = False

if(1<=n<=9999999):
    for i in range(6, -1, -1):
        id = n%(10**(i+1))
        if(i==0):
            if(id!=0):
                if(deal_l1):
                    s = s + " " + level[0]
                    deal_l1 = False
                else:
                    s = s + " " + num[id]+ " " + level[0]
            else:
                s = s + " " + level[0]
        elif(i%3==0):
            id = id//(10**i)
            if(id!=0):
                if(deal_l1):
                    s = s + " " + level_2[i//3]
                    deal_l1 = False
                else:
                    s = s + " " + num[id] + " " + level_2[i//3]
        elif(i%3==2):
            id = id//(10**i)
            if(id!=0):
                s = s + " " + num[id] + " " + level_2[0]
        elif(i%3==1):
            iid = id//(10**(i-1))
            id = id//(10**i)
            if(id!=0):
                if(id==1 and (iid!=0)):
                    iid = iid%10
                    s = s + " " + level_1[iid]
                    deal_l1 = True
                else:
                    s = s + " " + level[id]
    if(n>1):
        s += 's'
    print(s.strip())
else:
    print("out of range")