'''
說明
請撰寫一程式，訂義一與範例shelf相同的容器，內有5個字串代表5本書
請練習將自己面前shelf書櫃裡及掉到地上的書按照次序排列整齊。
有兩種作法

方法一 手動排序
將shelf 中第 3 個元素加到並新增到串列之後
將shelf 中第 3 個元素更改為Book3
方法二 自動排序
加入Book3之後使用 list.sort() 排序
Python
shelf = ["Book1", "Book2", "Book6", "Book4", "Book5"]
#...
print(shelf) 　　　　　# 輸出結果
C++
string books[] = {"Book1", "Book2", "Book6", "Book4", "Book5"};
list<string> st(books,books+5);
//...
printList(st); 　　　　　# 輸出結果
Input Format
NO

Output Format
排序後的數列
'''

shelf = ["Book1", "Book2", "Book6", "Book4", "Book5"]

#...
shelf.append(shelf[2])
shelf.pop(2)
shelf.insert(2, "Book3")



print(shelf) # 輸出結果