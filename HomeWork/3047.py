'''
說明
請撰寫一程式，可以令使用者輸入一整數n
並印出一n*n之*號方塊

Input Format
數字n

Output Format
長寬為n的*號方塊(每個*號後含一空白)

'''

n = int(input())

for i in range(n):
    print("* " * n)