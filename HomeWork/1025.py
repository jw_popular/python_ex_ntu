'''
說明
小傑將想要自己的生日設定為電腦的密碼，以知小傑的生日為85/08/12。
請完成一程式，讓使用者輸入純數字的密碼並將預設密碼設為850812，輸入正確時顯示welcome back，錯誤時顯示permission denied

Input Format
六位數整數數字

Output Format
是否成功登入的提示字串

'''

b_day=850812

input = eval(input())

if(input==b_day):
    print("welcome back")
else:
    print("permission denied")