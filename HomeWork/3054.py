'''
說明
請讓使用者輸入一數列，以append加入list當中(-1結束，list中不含-1)
可以使用內建函式或方法完成以下要求：

將其由小到大排序後印出
計算長度
計算加總
計算平均(印到小數點後2位)
計算最大值與其位置 (從1起算，若有相同的，以索引值小的為準)
計算最小值與其位置 (從1起算，若有相同的，以索引值小的為準)
印出原本未經排序的list
Input Format
一數列 (-1結束)

Output Format
將其由小到大排序後印出
計算長度
計算加總
計算平均(印到小數點後2位)
計算最大值與其位置 (從1起算，若有相同的，以索引值小的為準)
計算最小值與其位置 (從1起算，若有相同的，以索引值小的為準)
印出原本未經排序的list
'''

folder = []

f_sum = 0
max = [0, -1]
min = [0, -1]

while(True):
    val = eval(input())
    if(val==-1):
        break
    
    folder.append(val)
    f_sum+=val

    if(max[1]==-1 and min[1]==-1):
        max=[val, 1]
        min=[val, 1]
    else:
        if(max[0]<val):
            max=[val, len(folder)]
        if(min[0]>val):
            min=[val, len(folder)]

print(sorted(folder))

f_len = len(folder)
print(f_len)
print(f_sum)

f_avg = f_sum/f_len
print("%.2f"%f_avg)


print("%s@%d"%(str(max[0]), max[1]))
print("%s@%d"%(str(min[0]), min[1]))
print(folder)
