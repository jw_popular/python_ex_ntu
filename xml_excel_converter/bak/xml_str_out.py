from xml.dom import minidom

from bs4 import BeautifulSoup # pip install bs4 lxml

xmldoc = minidom.parse('xml_excel_converter/items.xml')
root = xmldoc.documentElement
itemlist = root.getElementsByTagName('item')
print("Number of item", len(itemlist))
print(itemlist)
#print("------------------")
for x in itemlist:
    print(x.nodeName)
    print(" ", x.toxml('utf-8'))
    nameNode = x.getElementsByTagName('item')
    print("==>",nameNode)
    #print(x.nameNode.childNodes)

print("------------------")
print(itemlist[0].attributes['name'].value)
print("------------------")
for s in itemlist:
    print(s.attributes['name'].value)
print("------------------")
#for s in itemlist:
#    print(s)

print("--------BeautifulSoup----------")
infile = open("xml_excel_converter/items.xml","r")
contens = infile.read()
src = BeautifulSoup(contens,'xml')
print(src.original_encoding)
print("------------------")

titles = src.find_all('item')

for title in titles:
    print(title.get('name'))
print("------------------")
for title in titles:
    print(title.get_text())
print("------------------")
for title in titles:
    print(title)
print("-------end--------")
