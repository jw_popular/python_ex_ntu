# Wellcome PYON368106

### 課程簡介 https://jgirl.ddns.net/hp/Python/ppt/00_introduction.pdf
* python_01簡介+安裝環境 https://jgirl.ddns.net/hp/Python/ppt/01_install.pdf
* python_02基本語法 https://jgirl.ddns.net/hp/Python/ppt/02_basic.pdf
* python_03變數與運算 https://jgirl.ddns.net/hp/Python/ppt/03_expression.pdf
	HW1: Page32
* python_04流程控制 https://jgirl.ddns.net/hp/Python/ppt/04_flowControl.pdf
	HW2: Page39
	HW3: Page77
	HW4: Page84
* python_05字串處理 https://jgirl.ddns.net/hp/Python/ppt/05_string.pdf
	HW5: Page73
* python_06序對,串列,集合,字典 https://jgirl.ddns.net/hp/Python/ppt/06_container.pdf
	HW6: Page68
* python_07函數與資料結構 https://jgirl.ddns.net/hp/Python/ppt/07_function.pdf
	HW7: Page34
* python_08模組 https://jgirl.ddns.net/hp/Python/ppt/08_module.pdf
* python_09例外處理與檔案處理 https://jgirl.ddns.net/hp/Python/ppt/09_fileAndException.pdf
* python_10類別 https://jgirl.ddns.net/hp/Python/ppt/10_class.pdf
	HW8: Page16
### 延申閱讀
* python_ext類別II (Extra) https://jgirl.ddns.net/hp/Python/ppt/python_ext%20class%20II.pdf
* python_ext Pyqt (Extra) https://jgirl.ddns.net/hp/Python/ppt/python_ext%20Pyqt.pdf
### 參考資料
* 指令小抄-python2.4 https://jgirl.ddns.net/hp/QuickReferenceCard/PQRC-2.4-A4-latest.pdf
* 指令小抄-python2.7 https://jgirl.ddns.net/hp/QuickReferenceCard/pqr2-7_printing_a4.pdf
* 指令小抄-python3 https://jgirl.ddns.net/hp/QuickReferenceCard/mementopython3-english.pdf

回家作業
Hw1:
單位換算-打(Dozen.py) 練習
Hw2:
判斷成績是否及格III(score3.py) 練習
Hw3:
猜數字進階版-終極密碼(guess_adv.py) 練習
Hw4:
2~m質數判斷程式(prime.py) 練習
Hw5:
找出文章中出現最多的字及總字數(str.py) 練習
Hw6:
集合功能完成問題Set 集合-及格名單(set.py) 練習
Hw7:
成績計算與列印-二維陣列-輸入版-找出2維list中平均最高的學生(heighest_avg.py) 練習
Hw8:
類別練習-找出平均最高分(student.py) 練習
Final:期末專題程式設計
除規定不可報告的題目外，主題不限，任何用python寫的程式均可
可用python解決你工作或日常生活的問題
第三方套件的使用或Python遊戲開發等
大概講解一下程式的目的與程式碼的架構
程式碼不用完全是自己寫的沒關系
若有參考網路上或其它的資料，請務必註明出處及網址
