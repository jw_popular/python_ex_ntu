##################################################################
# XML to Excel tool
#  
# input file:  the xml files from TSReader exported
# output file: output_excel_tsreader.xlsx
#
##################################################################
#from ast import IsNot
#from cmath import isnan
import os
import sys
import platform
import shutil

from xml.dom import minidom
#from openpyxl import load_workbook
from openpyxl import Workbook  # pip install openpyxl

from os.path import exists

#root_path = "xml_tsreader_analyse/"
root_path = os.getcwd()

#import_file = ("NIT_add_261M.xml", "NIT_add_267M.xml", "NIT_add_273M.xml", "LAB_549M.xml", "ORIGIN_NIT_261M.xml", "TEST998.xml", "2HR_CUT.xml")
#import_file = ("NIT_add_261M.xml", "LAB_549M.xml", "TEST998.xml", "2HR_CUT.xml")
import_file = ("LAB_261M_1110.xml", "LIVE_261M_1110.xml")

temp_file = root_path + "/tmp_tsreader.xml"

output_excel_file = root_path + "/output_excel_tsreader_1110.xlsx"

os_name = platform.system()


if os_name=="Linux":
    print("OS:",os_name)
    print(root_path)
elif os_name=="Windows":
    print("OS:",os_name)
    print(root_path)
else:
    sys.exit("Unknown OS: " + os_name)
    exit(1)

tb = ("PAT", "PMTs", "SDT", "NIT")

def parse_xml_SDT(sdt_tb, sb, input_file):
    
    iFile = root_path + "/" + input_file

    shutil.copyfile(iFile, temp_file)

    f = open(temp_file,"r",encoding = "utf-8")

    r = f.read()

    text = str(r.encode('utf-8'),encoding = "utf-8")

    DOMTree = minidom.parseString(text)

    collection = DOMTree.documentElement

    #collectionStr = "Collection:"+collection.nodeName
    #print(collectionStr)

    table_name = collection.getElementsByTagName(sdt_tb)

    if table_name:
        #print(sdt_tb, "is exist!")
        pass
    else:
        print(sdt_tb, "is empty!")
        print("!!!Wrong!!!")
        f.close()
        os.remove(temp_file)
        exit(1)

    if table_name.length != 1:
        print("Multiple NITs")

    for t in table_name:
        # ELEMENT_NODE, ATTRIBUTE_NODE, TEXT_NODE, CDATA_SECTION_NODE, 
        # ENTITY_NODE, PROCESSING_INSTRUCTION_NODE, COMMENT_NODE, 
        # DOCUMENT_NODE, DOCUMENT_TYPE_NODE, NOTATION_NODE
        sdt_entry_cnt = 0
        for n_list in t.childNodes:
            if n_list.nodeType == minidom.Node.ELEMENT_NODE:
                lst = []
                if n_list.nodeName == "VERSION":
                    print("SDT Version:", n_list.firstChild.data)
                    lst.append("SDT Version")
                    lst.append(n_list.firstChild.data)
                    if len(n_list.childNodes) != 1:
                        print("??? Multiple SDT Version ???")
                        lst.append("Multi-Version")
                    sb.append(lst)
                elif n_list.nodeName == "SDT-PID":
                    print("SDT PID:", n_list.firstChild.data)
                    lst.append("SDT PID")
                    lst.append(n_list.firstChild.data)
                    if len(n_list.childNodes) != 1:
                        print("??? Multiple SDT PID ???")
                        lst.append("Multi-PID")
                    sb.append(lst)
                elif n_list.nodeName == "SDT-ENTRY":
                    if sdt_entry_cnt == 0:
                        lst_title = ["SERVICE-ID", "SHORT-NAME", "TS ID", "ONID", "ACTUAL-TRANSPORT-STREAM", "SERVICE_TYPE"]
                        sb.append(lst_title)
                    sdt_entry_cnt += 1
                    n_val = []
                    for n_entry in n_list.childNodes:
                        if n_entry.nodeName == "SERVICE-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "SHORT-NAME":   # this is text node
                            if len(n_entry.childNodes)==0:
                                lst.append("")
                            else:
                                lst.append(n_entry.childNodes[0].nodeValue)
                        elif n_entry.nodeName == "TRANSPORT-STREAM-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "ORIGINAL-NETWORK-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "ACTUAL-TRANSPORT-STREAM":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "SDT-DESCRIPTORS":
                            for n_des in n_entry.childNodes:
                                if n_des.nodeName == "SERVICE":
                                    n_serv = n_des.getElementsByTagName("TYPE")
                                    for n_s in n_serv:
                                        lst.append(n_s.firstChild.data)
                                else:
                                    pass
                            sb.append(lst)
                        else:
                            if n_entry.nodeType != minidom.Node.TEXT_NODE:
                                pass
                else:
                    if n_list.nodeName == "CRC-ERRORS":
                        pass
                    else:
                        print(n_list.nodeName)
            else:
                if n_list.nodeType != minidom.Node.TEXT_NODE:
                    print("Other node:", n_list.nodeName)

        print("SDT Entry numbers:", sdt_entry_cnt)

    f.close()
    os.remove(temp_file)

def parse_xml_NIT(nit_tb, sb, input_file):
    
    iFile = root_path + "/" + input_file

    shutil.copyfile(iFile, temp_file)

    f = open(temp_file,"r",encoding = "utf-8")

    r = f.read()

    text = str(r.encode('utf-8'),encoding = "utf-8")

    DOMTree = minidom.parseString(text)

    collection = DOMTree.documentElement

    #collectionStr = "Collection:"+collection.nodeName
    #print(collectionStr)

    table_name = collection.getElementsByTagName(nit_tb)

    if table_name:
        #print(nit_tb, "is exist!")
        pass
    else:
        print(nit_tb, "is empty!")
        print("!!!Wrong!!!")
        f.close()
        os.remove(temp_file)
        exit(1)

    if table_name.length != 1:
        print("Multiple NITs")

    for t in table_name:
        # ELEMENT_NODE, ATTRIBUTE_NODE, TEXT_NODE, CDATA_SECTION_NODE, 
        # ENTITY_NODE, PROCESSING_INSTRUCTION_NODE, COMMENT_NODE, 
        # DOCUMENT_NODE, DOCUMENT_TYPE_NODE, NOTATION_NODE
        nit_entry_cnt = 0
        for n_list in t.childNodes:
            if n_list.nodeType == minidom.Node.ELEMENT_NODE:
                lst = []
                if n_list.nodeName == "VERSION":
                    print("NIT Version:", n_list.firstChild.data)
                    lst.append("NIT Version")
                    lst.append(n_list.firstChild.data)
                    if len(n_list.childNodes) != 1:
                        print("??? Multiple NIT Version ???")
                        lst.append("Multi-Version")
                    sb.append(lst)
                elif n_list.nodeName == "NIT-PID":
                    print("NIT PID:", n_list.firstChild.data)
                    lst.append("NIT PID")
                    lst.append(n_list.firstChild.data)
                    if len(n_list.childNodes) != 1:
                        print("??? Multiple NIT PID ???")
                        lst.append("Multi-PID")
                    sb.append(lst)
                elif n_list.nodeName == "NIT-ENTRY":
                    if nit_entry_cnt == 0:
                        lst_title = ["INDEX", "NID", "TS ID", "ONID", "FREQ", "NUMOFSERV", "SERVICE-ID"]
                        sb.append(lst_title)
                    nit_entry_cnt += 1
                    n_val = []
                    for n_entry in n_list.childNodes:
                        if n_entry.nodeName == "INDEX":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "NETWORK-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "TRANSPORT-STREAM-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "ORIGINAL-NETWORK-ID":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "FREQUENCY":
                            #print(n_entry.firstChild.data)
                            lst.append(n_entry.firstChild.data)
                        elif n_entry.nodeName == "TS-DESCRIPTORS":
                            for n_des in n_entry.childNodes:
                                if n_des.nodeName == "SERVICE-LIST":
                                    n_serv = n_des.getElementsByTagName("SERVICE")
                                    lst.append(len(n_serv))
                                    for n_s in n_serv:
                                        n_serv_id = n_s.getElementsByTagName("ID")
                                        for n_s_id in n_serv_id:
                                            #print(n_s_id.firstChild.data)
                                            lst.append(n_s_id.firstChild.data)
                                else:
                                    pass
                            sb.append(lst)
                        else:
                            if n_entry.nodeType != minidom.Node.TEXT_NODE:
                                pass
                else:
                    if n_list.nodeName == "CRC-ERRORS":
                        pass
                    else:
                        print(n_list.nodeName)
            else:
                if n_list.nodeType != minidom.Node.TEXT_NODE:
                    print("Other node:", n_list.nodeName)

        print("NIT Entry numbers:", nit_entry_cnt)

    f.close()
    os.remove(temp_file)


wb = Workbook()
#print(wb.sheetnames)

for k in range(len(import_file)):
    iFile = root_path + "/" + import_file[k]
    sb_title_nit = import_file[k].split(sep='.')[0] + "_NIT"
    sb_title_sdt = import_file[k].split(sep='.')[0] + "_SDT"
    if exists(iFile):
        if k==0:
            #if(len(wb.sheetnames)==1 and wb.sheetnames[0]=="Sheet"):
            #    print("Initial workbook!!")
            actSheet = wb.active
            actSheet.title = sb_title_nit
            wb.create_sheet(sb_title_sdt)
            actSheet2 = wb[sb_title_sdt]
        else:
            wb.create_sheet(sb_title_nit)
            actSheet = wb[sb_title_nit]
            wb.create_sheet(sb_title_sdt)
            actSheet2 = wb[sb_title_sdt]
        print("Work Book:", actSheet.title, "&", actSheet2.title)
        parse_xml_NIT(tb[3], actSheet, import_file[k])
        parse_xml_SDT(tb[2], actSheet2, import_file[k])
    print(wb.sheetnames)

wb.save(output_excel_file)

print("Done!!")